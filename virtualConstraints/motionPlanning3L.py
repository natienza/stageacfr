#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 14 09:36:21 2019

@author: natienza
"""

import numpy as np
import matplotlib.pyplot as plt

#SIMULATION SETUP
T_SIMU = 10 #[s]
NB_SAMPLE = 3000
STEP = (T_SIMU+1)/NB_SAMPLE

#MODEL PARAMETERS
gamma = 1.
beta = 1.

#SIMULATIOM
theta = [6*np.pi/8]
dtheta = 1

t = np.linspace(0,T_SIMU,NB_SAMPLE) #[s]
for i in range(int(NB_SAMPLE)-1):
    new_value = np.sqrt(dtheta**2 + 2*gamma/beta*(np.cos(theta[-1]) - np.cos(theta[0])))
    theta += [(theta[-1]+new_value*STEP)%(2*np.pi)]

plt.figure()
plt.plot(t,theta)

