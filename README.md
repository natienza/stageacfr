Stage ACFR
==

Ce dépot répertorie l'ensemble des codes écrits lors d'un stage à l'université de Sydney au _Australian  center for field robotics_ sous la direction du professeur Ian Manchester. L'objet de ce stage a été l'étude d'algorithme de contrôle d'un robot bipède simple. Le mémoire du stage peut être accessible [ici](https://owncloud.nisah.fr/index.php/s/5zRxd5CocojFNcE ). Le stage s'est organisé en trois parties :

* Développement d'un environnement de simulation physique : `./simulation3L`

* Développement d'un algorithme de contrôle de marche reposant sur l'expression de contraintes virtuels et quelques réflexion quand à l'optimisation des paramètres de ces contraintes : `./virtualConstraints`,  `./optimisedParam`. 

*  Développement d'une discrétisation du système physique étudié et développement d'algorithmes d'apprentissage renforcé dans le but d'apprendre à marcher sur des terrains compliqués : `./SARSA_3L`

Des rendus vidéos des différentes simulations sont disponibles dans les dossiers des parties concernées.

Dépendances
--

Seules les bibliothèques `numpy`, `matplotlib` et `openCV` ont été utilisées lors de ce stage. `openCV` ayant juste été utilisé dans le rendu des video pour le rapport, les codes peuvent être éxécutés sans. Les codes ont été écrit sous python3.

