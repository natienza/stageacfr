#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  1 11:41:52 2019

@author: natienza

"""

#from math import factorial
from scipy.integrate import odeint
import numpy as np
from numpy.linalg import inv
from numpy.linalg import pinv
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.animation as animation
from math import factorial

def sgn(x):
    if x==0:
        return 1
    else:
        return x/abs(x)

class Simulation() :

    def __init__(self):
        ## SIMULATION PARAMETERS
        self.time = []
        self.step = 0
        self.currentTime = 0
        ## MODEL OF ROBOT
        self.walker = Walker()
        ## WHO AM I
        self.name = 'Simulation of ' + self.walker.name

    
    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name
    
    def launch(self, duration = 30, step = 0.1, q0 = np.zeros((2,1)), qDot0 = np.zeros((2,1)), *args, **kwargs):
        mode = kwargs.get('mode')
        control = kwargs.get('control')
        self.walker.anim.step = step
        self.step = step
        self.time = np.linspace(0,duration,int(duration/step)+1)
        self.walker.setIC(q0, qDot0)
        for t in self.time:
            if t == 0:
                pass
            if mode == 'euler':
                if not(self.walker.simulateEuler(step,t,control)):
                    #print("DV")
                    return 0
            elif mode == 'RK' :
                self.walker.simulateRK(step,t,control)
            elif mode == 'scipy':
                self.walker.simulateScipy(step,t,control)
            else :
                print('mode integration non connu')
                return 0
        return
        

class Walker() :
    
    def __init__(self, *args, **kwargs):
        ## WHO AM I
        self.name = '3L Compass gait biped walker'
        ## GEOMETRIC PARAMETERS
        self.a = 1.5
        self.b = 1.5
        self.l = self.a + self.b
        self.mH = 5.07#2
        self.m = 5#3/4
        ## GENERALIZED COORDINATES
        self.q = np.array([ [0],
                            [0]], dtype = np.float64)
        self.qDot = np.array([ [0],
                               [0]], dtype = np.float64)
        self.xq = np.array([0, 0, 0, 0], dtype = np.float64)
        ## STATE SPACE PARAMETERS
        self.x = np.array([ 0,0,0,0], dtype = np.float64)
        self.u = np.array([0],dtype = np.float64)
        self.a1 = 25#40
        self.a0 = 10#65
        self.y = 0
        self.v = 0
        ## CONTROL
        self.controlMode = kwargs.get('controlMode') if kwargs.get('controlMode')!=None else 'feedbackLinearisation'
        ## ENERGY
        self.k = 0
        self.V = 0
        ## DYNAMIC SINGLE SUPPORT MODEL
        self.M = np.array([ [self.mH*self.l**2+self.m*self.b**2+self.m*self.l**2, -self.m*self.l*self.a*np.cos(self.x[1])],
                            [-self.m*self.l*self.a*np.cos(self.x[1]), self.m*self.a**2]])
        
        self.C = np.array([ [0, self.m*self.l*self.a*self.qDot[1,0]*np.sin(self.x[1])],
                            [self.m*self.l*self.a*self.qDot[0,0]*np.sin(-self.x[1]), 0]])
    
        self.g = np.array([ [-(self.m*self.b + self.m*self.l + self.mH*self.l)*9.81*np.sin(self.x[0])],
                            [self.m*self.a*9.81*np.sin(self.q[1,0])]])
        ## IMPACT TRANSITION MATRIX
        self.psi = 0
        self.contact = 0 #contact flag
        self.oldq = np.array([ [0],
                            [0]], dtype = np.float64)
        self.eps = 0.001 #epsilon on the contact surface (disrete system)
        self.P = np.array([ [0, 1],
                            [1, 0]])
        self.Pq = np.linalg.solve(np.array([ [self.m*self.a**2 - self.m*self.a*self.l*np.cos(self.x[1]), self.m*self.l**2+self.mH*self.l**2+self.m*self.b**2 - self.m*self.a*self.l*np.cos(self.x[1])],
                                        [self.m*self.a**2, -self.m*self.a*self.l*np.cos(self.x[1])]], dtype = np.float64),np.array([ [(self.m*self.l*self.b + self.mH*self.l**2 + self.m*self.l*self.b)*np.cos(self.x[1]), -self.m*self.a*self.b],
                                                                                                                                        [-self.m*self.a*self.b, 0]], dtype = np.float64))
        ## VIRTUAL CONSTRAINTS
        self.lStep = 2
        self.tStep = 0.2
        self.param = [ -2*np.arcsin(self.lStep/(2*self.l)), -2*np.arcsin(self.lStep/(2*self.l)) + 4/3*np.arcsin(self.lStep/(2*self.l)), 2*np.arcsin(self.lStep/(2*self.l)), 2*np.arcsin(self.lStep/(2*self.l))  ]
        ## LINEAR MODEL
        self.N = 5
        self.A = np.array([[1, 0.001],
                          [0, 1]])
        self.B = np.array([[0],
                           [0.001]])
        self.init_A_e()
        ## DEBUGG ATTRIBUTES
        self.nbContacts = 0
        self.nbApprox = 0
        self.plotDiff = Graph()
        self.flag = 0
        self.t = 0
        ## PLOT
        self.phasePortraitX = Graph('$\psi$ versus $q_1$', '$q_1$ [rad]', '$\psi$ [rad]')
        self.phasePortrait = Graph('Phase portrait','$q$ [rad]','$\dot{q}$ [rad/s]', '$q_1$ (stance leg)', '$q_2$ (swing leg)')
        self.genCoordTimeEvolution = Graph('State time evolution','t [s]','q [rad]', '$q_1$ (stance leg)', '$q_2$ (swing leg)' )
        self.speedGenCoordTimeEvolution = Graph('Generalized speed versus time', 't [s]', '$\dot{q}$ [rad/s]')
        self.command = Graph('Control input (hip torque versus time)', 't [s]', 'u [Nm]')
        self.inputEnergy = Graph('Input Energy', 't [s]', '$u^2$ [$Nm^2$]')
        self.obs = Graph('Observation output', 't [s]', 'y [rad]')
        self.obsPsi = Graph('$\psi$ versus time', 't [s]', '$\psi$ [rad]')
        self.commandeLin = Graph('linear command input', 't [s]', 'v [Nm]')
        self.energy = Graph('Energy versus time', 't [s]', 'Energy [J]', '$K$', '$V$', '$V+K$', plot3Style = '--')
        self.poincare = Graph('Poincare map', '$q_1$ [rad]', '$q_2 [rad]$', plot1Style = '--o')
        self.commandPredicted = Graph('Prediction','t [s]', 'u' )
        self.convergence = Graph('', 't [s]', '# iteration')
        ## ANIMATION
        self.anim = Animation(self.psi)
        self.anim.nbBody = 2
        
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.name

    def getStateVar(self):
        return self.x
    
    def getGenCoord(self):
        return self.q,self.qDot
    
    def f(self,y ,t):
        invM = self.inverse(self.M)
        dq = - np.dot(invM,self.C).dot(self.qDot) - invM.dot(self.g)
        return np.array([[self.qDot[0,0]],
                         [self.qDot[1,0]],
                         [dq[0,0]],
                         [dq[1,0]]])
    
    def setIC(self, q0, qDot0):
        self.q = q0
        self.oldq = q0
        self.qDot = qDot0
        self.xq[0] = q0[0,0]
        self.xq[1] = q0[1,0]
        self.xq[2] = qDot0[0,0]
        self.xq[3] = qDot0[1,0]
        self.x[0] = self.q[0,0]
        self.x[1] = self.q[1,0] - self.q[0,0]
        self.x[2] = self.qDot[0,0]
        self.x[3] = self.qDot[1,0] - self.qDot[0,0]
        self.updateDynamicParameters()
        #print('IC set')
        return 1 
    
    def updateDynamicParameters(self, *args, **kwargs):
        if len(kwargs) != 0:
            X = kwargs.get('X')
            ''' X = np.array([q1,psi,q1Dot, psiDot])'''
            self.x = X
            self.xq = np.array([self.x[0], self.x[1]+self.x[0], self.x[2], self.x[2]+self.x[3]])
            self.q = np.array([[self.xq[0]],[self.xq[1]]])
            self.qDot = np.array([[self.xq[2]], [self.xq[3]]])
        ## DYNAMIC SINGLE SUPPORT MODEL
        self.M = np.array([ [self.mH*self.l**2+self.m*self.b**2+self.m*self.l**2, -self.m*self.l*self.a*np.cos(self.x[1])],
                            [-self.m*self.l*self.a*np.cos(self.x[1]), self.m*self.a**2]], dtype = np.float64)
        
        self.C = np.array([ [0, self.m*self.l*self.a*self.qDot[1,0]*np.sin(self.x[1])],
                            [self.m*self.l*self.a*self.qDot[0,0]*np.sin(-self.x[1]), 0]], dtype = np.float64)
    
        self.g = np.array([ [-(self.m*self.b + self.m*self.l + self.mH*self.l)*9.81*np.sin(self.x[0])],
                            [self.m*self.a*9.81*np.sin(self.q[1,0])]], dtype = np.float64)
        ## ENERGY
        self.k = 0.5 * self.m * ( self.b * self.x[2]**2 + (self.l*self.x[2]*np.sin(self.x[0]) + self.a*self.qDot[1,0]*np.sin(self.q[1,0]) )**2 + (self.l*self.x[2]*np.cos(self.x[0]) + self.a*self.qDot[1,0]*np.cos(self.q[1,0])) **2  ) + 0.5*self.mH*self.qDot[0,0]**2
        self.V =0.5 * self.m * (self.b + self.l) * np.cos(self.x[0]) + 0.5 * self.mH * (self.l*np.cos(self.x[0]) - self.a*np.cos(self.q[1,0]))
    
    def updateContactTransition(self):
        ## IMPACT TRANSITION MATRIX
        self.Pq = np.linalg.solve(np.array([ [self.m*self.a**2 - self.m*self.a*self.l*np.cos(self.x[1]), self.m*self.l**2+self.mH*self.l**2+self.m*self.b**2 - self.m*self.a*self.l*np.cos(self.x[1])],
                                        [self.m*self.a**2, -self.m*self.a*self.l*np.cos(self.x[1])]], dtype = np.float64).dot(self.P),np.array([ [(self.m*self.l*self.b + self.mH*self.l**2 + self.m*self.l*self.b)*np.cos(self.x[1]), -self.m*self.a*self.b],
                                                                                                                                        [-self.m*self.a*self.b, 0]], dtype = np.float64))

    def surfaceContact(self):
        if (np.cos(self.oldq[0,0]+self.psi) - np.cos(self.oldq[1,0]+self.psi))*(np.cos(self.q[0,0]+self.psi) - np.cos(self.q[1,0]+self.psi)) <= 1e-5 and self.qDot[0,0]<=0:
            if abs(self.q[0,0] - self.q[1,0]) <= 0.2:
                return 0 # false contact : the two end points are colluding
            if  (-self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0])) < 0 :
                return 0  # false contact : we want to go forward
            if abs(-self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0])) < 0.3:
                return 0 # false contact
            ## POINCARE MAP UPDATE
            self.poincare.x1.append(self.x[0])
            self.poincare.y1.append(self.x[2])
            ## ANIMATION UPDATE
            self.contact = 1
            ## CONTACT TRANSITION MODEL
            self.updateContactTransition()
            self.q = self.P.dot(self.q)
            self.qDot = self.Pq.dot(self.qDot)
            self.xq = np.array([self.q[0,0], self.q[1,0], self.qDot[0,0], self.qDot[1,0]])
            self.x = np.array([self.q[0,0],self.q[1,0]-self.q[0,0], self.qDot[0,0], self.qDot[1,0]-self.qDot[0,0]])
            self.nbContacts += 1
            return 1
        return 0
    
    def updatePlot(self,t):
        self.phasePortrait.x1.append(self.q[0,0])#%(2*np.pi))
        self.phasePortrait.y1.append(self.qDot[0,0])
        self.phasePortrait.x2.append(self.q[1,0])#%(2*np.pi))
        self.phasePortrait.y2.append(self.qDot[1,0])
        self.genCoordTimeEvolution.x1.append(t)
        self.genCoordTimeEvolution.y1.append(self.q[0,0])#%(2*np.pi))
        self.genCoordTimeEvolution.x2.append(t)
        self.genCoordTimeEvolution.y2.append(self.q[1,0])#%(2*np.pi))
        self.speedGenCoordTimeEvolution.x1.append(t)
        self.speedGenCoordTimeEvolution.y1.append(self.qDot[0,0])
        self.speedGenCoordTimeEvolution.x2.append(t)
        self.speedGenCoordTimeEvolution.y2.append(self.qDot[1,0])
        self.plotDiff.x1.append(t)
        self.plotDiff.y1.append((np.cos(self.oldq[0,0]) - np.cos(self.oldq[1,0]))*(np.cos(self.q[0,0]) - np.cos(self.q[1,0])))
        self.phasePortraitX.x1.append(self.x[0])
        self.phasePortraitX.y1.append(self.x[1])
        self.command.x1.append(t)
        self.command.y1.append(self.u[0])
        self.inputEnergy.x1.append(t)
        self.inputEnergy.y1.append(self.u[0]**2)
        self.obs.x1.append(t)
        self.obs.y1.append(self.x[1] - self.psiD(self.x[0]))
        self.obsPsi.x1.append(t)
        self.obsPsi.y1.append(self.x[1])
        self.commandeLin.x1.append(t)
        self.commandeLin.y1.append(self.v)
        #self.commandeLin.x2.append(t)
        #self.commandeLin.y2.append(self.vvv)
        #self.commandeLin.x3.append(t)
        #self.commandeLin.y3.append(self.v)
        self.energy.x1.append(t)
        self.energy.y1.append(self.k)
        self.energy.x2.append(t)
        self.energy.y2.append(self.V)
        self.energy.x3.append(t)
        self.energy.y3.append(-self.k+self.V)
        #self.commandPredicted.x1.append(t)
        #self.commandPredicted.y1.append(self.vv)
        ## UPDATE ANIMATION
        self.updateAnimation(t)
        return         
    
    def fsp2(self, X, t):
        ''' X = np.array([q1,psi,q1Dot, psiDot])'''
        self.updateDynamicParameters(X=X)
        dq = np.array([[X[2]],
                       [X[2]+X[3]]])
        invM = self.inverse(self.M)
        ddq = -invM.dot(self.C.dot(dq) + self.g)
        ddx = np.array([ddq[0,0], ddq[1,0]-ddq[0,0]])
        #self.predictParam(X,ddx)
        if self.control and (t/0.001)%5==0:
            if self.controlMode == 'feedbackLinearisation':
                self.v = -self.a0 * (X[3] - self.psiDDot(X[0])*X[2]) - self.a1*(X[1]-self.psiD(X[0]))
                #self.vv = self.predict(X[1]-self.psiD(X[0]),X[3] - self.psiDDot(X[0])*X[2])
                #self.vvv = self.predict2(X[1]-self.psiD(X[0]),X[3])
                #self.uu = self.predictU3(X,ddx)
                self.u[0] = -ddx[1] + self.v + self.psiDDotDot(X[0])*X[2]**2 + self.psiDDot(X[0])*ddx[0]
                #self.vv = self.predict(X[1] - 0.6, X[3])
                #self.u[0] = -ddx[1] + self.vv
            elif self.controlMode == 'PD':
                self.u[0] = -50 * (self.x[1] - np.arcsin(self.lStep/(2*self.l))) - 10*self.x[3]
            if self.u[0] > 100:
                self.u[0] = 100
            elif self.u[0]<-100:
                self.u[0] = -100
            ddx = np.array([ddx[0], ddx[1] + self.u[0]])
        return np.array([X[2], X[3], ddx[0], ddx[1]])
    
    def simulateEuler(self, step, t, mode='passiv'):
        for i in self.x:
            if i>100:
                return 0
        if mode == 'passiv':
            self.control = False
        else :
            self.control = True
        ## SURFACE DE CONTACT
        self.surfaceContact()
        ## EULER INTEGRATION
        self.x =  (self.x + step*self.fsp2(self.x,t))
        self.xq = np.array([self.x[0], self.x[1]+self.x[0], self.x[2], self.x[2]+self.x[3]])
        ## DYNAMIC MODEL EVOLUTION
        self.oldq = self.q.copy()
        self.q = np.array([[self.xq[0]],[self.xq[1]]])
        self.qDot = np.array([[self.xq[2]], [self.xq[3]]])
        self.updateDynamicParameters()
        ## UPDATE PLOT
        self.updatePlot(t)
        return 1

    def simulateRK(self, step, t, mode='passiv'):
        if mode == 'passiv':
            self.control = False
        else :
            self.control = True
        ## SURFACE DE CONTACT
        self.surfaceContact()
        ## CONTROL
        self.controlFeedback()
        if np.linalg.det(self.M) == 0:
            print('ERROR MASS MATRIX INVERSION -- DEBUGG') #Should never happen according to spectral theorem
            invM = pinv(self.M)#SVD approx
            self.nbApprox += 1
        else:            
            invM = inv(self.M)    
        ## DYNAMIC MODEL
        self.oldq = self.q.copy()
        def f(X):
            dq = np.array([[X[2,0]],
                           [X[3,0]]], dtype = np.float64)
            dq += -invM.dot(self.C.dot(dq) + self.g)
            return np.array([[X[2,0]],
                             [X[3,0]],
                             [dq[0,0]],
                             [dq[1,0]]], dtype = np.float64)
        ## RUNGE-KUTTA INTEGRATION (order 4)
        Xq = np.array([[self.q[0,0]],
                       [self.q[1,0]],
                       [self.qDot[0,0]],
                       [self.qDot[1,0]]], dtype = np.float64)
        k1 = f(Xq)
        k2 = f(Xq+step*k1/2)
        k3 = f(Xq+step*k2/2)
        k4 = f(Xq + step*k3)
        Xq = Xq + step/6 * (k1 + 2*k2 + 2*k3 + k4)
        ## UPDATING
        self.q[0,0] = Xq[0,0]
        self.q[1,0] = Xq[1,0]
        self.qDot[0,0] = Xq[2,0]
        self.qDot[1,0] = Xq[3,0]
        self.x[0,0] = self.q[0,0] 
        self.x[1,0] = self.q[1,0] - self.q[0,0]
        self.x[2,0] = self.qDot[0,0]
        self.x[3,0] = self.qDot[1,0] - self.qDot[0,0] + b*self.u
        self.updateDynamicParameters()
        ## UPDATE PLOT
        self.updatePlot(t)
        return 1
    
    def fsp(self,X,t):
        ''' X = np.array([q1, q2, q1Dot, q2Dot])'''
        self.updateDynamicParameters(X = X)
        invM = self.inverse(self.M)
        dq = np.array([[X[2]],
                      [X[3]]])
        dq = -invM.dot(self.C.dot(dq) + self.g).copy()
        if self.control :
            self.v = -self.a0 * (X[3] - X[2] - self.psiDDot(X[0])*X[2]) - self.a1 * (X[1]-X[0] - self.psiD(X[0]))
            self.u = np.array([ -(dq[1,0] - dq[0,0]) + self.v + self.psiDDot(X[0])*dq[0,0] + self.psiDDotDot(X[0]) * X[2]**2])
            dq += np.array([[-self.u[0]], [self.u[0]]])
        return np.array([X[2], X[3], dq[0,0], dq[1,0]])
        
    
    def simulateScipy(self,step, t, mode = 'passiv'):
        if mode == 'passiv':
            self.control = False
        else :
            self.control = True
        ## CONTACT MAP
        self.surfaceContact()
        ## INTEGRATION
        integ = odeint(self.fsp2, self.x, np.array([t-step, t]))
        self.x = integ[-1,:].copy()
        ## UPDATING
        self.xq = np.array([self.x[0], self.x[1]+self.x[0], self.x[2], self.x[2]+self.x[3]])
        ## DYNAMIC MODEL EVOLUTION
        self.oldq = self.q.copy()
        self.q = np.array([[self.xq[0]],[self.xq[1]]])
        self.qDot = np.array([[self.xq[2]], [self.xq[3]]])
        self.updateDynamicParameters()
        ## UPDATE PLOT
        self.updatePlot(t)
        return 1
    
    
    def updateAnimation(self, t=0):
        lastRef = [self.anim.frames[-1][0][0],self.anim.frames[-1][1][0]]
        if self.contact == 1:
            self.contact = 0
            lastRef[0] -= -self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0])  
            lastRef[1] -= self.l*(np.cos(self.q[0,0]) - np.cos(self.q[1,0]))
            print(str(lastRef) + ' -> ' + str(t) )
        p1 = [-self.l*np.sin(self.q[0,0])+ lastRef[0], self.l*np.cos(self.q[0,0])+ lastRef[1]]
        p2 = [-self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0]) + lastRef[0], self.l*np.cos(self.q[0,0]) - self.l*np.cos(self.q[1,0]) + lastRef[1]]
        self.anim.frames.append([[lastRef[0],p1[0],p2[0]],[lastRef[1],p1[1],p2[1]]])          
        return 1
    
    def init_A_e(self):
        self.A_e = np.zeros((2*self.N,3*self.N))
        for i in range (0,2*self.N,2):
            self.A_e[i,i] = 1
            self.A_e[i+1, i+1] = 1
            if i>0:
                self.A_e[i,i-2] = -self.A[0,0]
                self.A_e[i,i-1] = -self.A[0,1]
                self.A_e[i+1,i-2] = -self.A[1,0]
                self.A_e[i+1,i-1] = -self.A[1,1]
            self.A_e[i+1][2*self.N+int(i/2)] = -self.B[1,0]
        self.H = np.zeros((3*self.N, 3*self.N))
        for i in range(3*self.N):
            if i>=2*self.N:
                 self.H[i,i] = 1
            if i%2 == 0:
                self.H[i,i] = self.a1
            else:
                self.H[i,i] = self.a0
    
    def predict(self,z1, z1Dot):
        b_e = np.zeros((2*self.N,2 ))
        b_e[0,1] = 0.001
        b_e[0,0] = 1
        b_e[1,1] = 1
        #b_e[0,0] = z1 + 0.001*z1Dot
        #b_e[1,0] = z1Dot
        inverse = inv(self.A_e.dot(inv(self.H).dot(self.A_e.transpose())))
        X = inv(self.H).dot(self.A_e.transpose().dot(inverse).dot(b_e)).dot(np.array([[z1],[z1Dot]]))
        #print( inv(self.H).dot(self.A_e.transpose().dot(inverse).dot(b_e))[self.N*2:-1, :])
        self.debugg = X
        #return -self.a1*z1 - self.a0*z1Dot
        return X[2*self.N,0]
    
    def predict2(self, z1, z1Dot):
        self.A1 = np.zeros((self.N*2, self.N*2))
        self.B1 = np.zeros((self.N*2,self.N))
        for i in range (0,2*self.N,2):
            self.A1[i,i] = 1
            self.A1[i+1, i+1] = 1
            if i>0:
                self.A1[i,i-2] = -self.A[0,0]
                self.A1[i,i-1] = -self.A[0,1]
                self.A1[i+1,i-2] = -self.A[1,0]
                self.A1[i+1,i-1] = -self.A[1,1]
            self.B1[i+1,int(i/2)] = -self.B[1,0]
        b_e = np.zeros((2*self.N, 1))
        b_e[0,0] = z1 + 0.001*z1Dot
        b_e[1,0] = z1Dot
        inverse = inv( (inv(self.A1).dot(self.B1)).transpose().dot(inv(self.A1).dot(self.B1)) )
        V = -inverse.dot((inv(self.A1).dot(self.B1)).transpose()).dot(inv(self.A1)).dot(b_e)
        return V[1,0]
    
    def predictU(self, X, ddX):
        ''' X = [q1, psi, q1Dot, psiDot]_k, ddX = [q1DotDot, psiDotDot]_k'''
        step = 0.001
        u = -ddX[1] - 1/step*( X[3] + self.psiDDot(X[0])*(X[2] + step*ddX[0]))
        return u
    
    def predictU2(self, X, ddX):
        ''' X = [q1, psi, q1Dot, psiDot]_k, ddX = [q1DotDot, psiDotDot]_k'''
        step = 0.001
        alpha = 10
        u = 0
        print('\n')
        while (X[3] + step*(ddX[1] + u) + self.psiDDot(X[0])*(X[2] + step*ddX[0]))**2 > 1:
            print((X[3] + step*(ddX[1] + u) + self.psiDDot(X[0])*(X[2] + step*ddX[0]))**2)
            d = 2*step*( X[3] + step*(ddX[1] + u) + self.psiDDot(X[0])*(X[2] + step*ddX[0]))
            u -= alpha*d
        return u
 
    def predictU3(self, X, ddX):
        ''' X = [q1, psi, q1Dot, psiDot]_k-1, ddX = [q1DotDot, psiDotDot]_k-1'''
        step = 0.001
        u = -1/step*( X[1] +2*step*X[3] + step**2*ddX[1] - 0.2)#self.psiD(X[0] + 2*step*X[2]) )
        return u  
    
    def predictParam(self, X, ddx):
        alpha = 0.0001
        self.param[1] = -2*np.arcsin(self.lStep/(2*self.l)) + 4/3*np.arcsin(self.lStep/(2*self.l))
        v = -self.a0 * (X[3] - self.psiDDot(X[0])*X[2]) - self.a1*(X[1]-self.psiD(X[0]))
        u = -ddx[1] + v + self.psiDDotDot(X[0])*X[2]**2 + self.psiDDot(X[0])*ddx[0]
        done = False
        cpt = 0
        while not(done):
            oldU = u
            du = u*(self.a0*self.d2(X[0])*X[2] + self.a1*self.d1(X[0]) + self.d1(X[0])*ddx[0] + self.d3(X[0])*X[2]**2 )
            oldParam = self.param[1]
            self. param[1] -= alpha*du
            if self.param[1] > self.param[0] - 2/3 *(-2*np.arcsin(self.lStep/(2*self.l))):
                self.param[1] = oldParam
                done = True
            v = -self.a0 * (X[3] - self.psiDDot(X[0])*X[2]) - self.a1*(X[1]-self.psiD(X[0]))    
            u = -ddx[1] + v + self.psiDDotDot(X[0])*X[2]**2 + self.psiDDot(X[0])*ddx[0]
            cpt += 1
            if abs(oldU**2 - u**2) < 0.1:
                done = True
        self.convergence.x1.append(self.t)
        self.convergence.y1.append(cpt)
        self.t += 0.001
    
    def predictParam2(self, X, ddx):
        alpha = 0.0001
        self.param[1] = -2*np.arcsin(self.lStep/(2*self.l)) + 4/3*np.arcsin(self.lStep/(2*self.l))
        v = -self.a0 * (X[3] - self.psiDDot(X[0])*X[2]) - self.a1*(X[1]-self.psiD(X[0]))
        u = -ddx[1] + v + self.psiDDotDot(X[0])*X[2]**2 + self.psiDDot(X[0])*ddx[0]
        du = u*(self.a0*self.d2(X[0])*X[2] + self.a1*self.d1(X[0]) + self.d1(X[0])*ddx[0] + self.d3(X[0])*X[2]**2 )
        oldDu = 0
        done = False
        while not(done):
            oldU = u
            oldParam = self.param[1]
            print(oldDu-du)
            self. param[1] -= 0.5*alpha*(oldDu-du)
            if self.param[1] > self.param[0] - 2/3 *(-2*np.arcsin(self.lStep/(2*self.l))):
                self.param[1] = oldParam
                done = True
            oldDu = du
            du = u*(self.a0*self.d2(X[0])*X[2] + self.a1*self.d1(X[0]) + self.d1(X[0])*ddx[0] + self.d3(X[0])*X[2]**2 )
            v = -self.a0 * (X[3] - self.psiDDot(X[0])*X[2]) - self.a1*(X[1]-self.psiD(X[0]))    
            u = -ddx[1] + v + self.psiDDotDot(X[0])*X[2]**2 + self.psiDDot(X[0])*ddx[0]
            if abs(oldU**2 - u**2) < 0.01:
                done = True
                
    
    def d1(self, q1):
        psiLim = -np.arcsin(self.lStep/(2*self.l))
        s = (q1 +psiLim ) / (2 * psiLim)
        h = 0
        M = 3
        h = M*s*(1-s)**(M-1)
        return h
    
    def d2(self, q1):
        psiLim = -np.arcsin(self.lStep/(2*self.l))
        s = (q1 +psiLim ) / (2 * psiLim)
        M = 3
        h = (1/(2*psiLim))*(M*(1-s)**(M-1) - M*(M-1)*s*(1-s)**(M-2))
        return h
    
    def d3(self,q1):
        psiLim = -np.arcsin(self.lStep/(2*self.l))
        s = (q1 +psiLim ) / (2 * psiLim)
        M = 3
        h = (1/(2*psiLim))**2 * ( -2*M*(M-1)*(1-s)**(M-2) + M*s)
        return h
        
    def psiD(self, q1) :
        psiLim = -np.arcsin(self.lStep/(2*self.l))
        s = (q1 +psiLim ) / (2 * psiLim)
        d = (2*psiLim)**2/(self.tStep*3)
        a = [2*psiLim,2*psiLim-d ,-2*psiLim +d,-2*psiLim]
        a = self.param
        h=0
        for i in range (4):
           h += a[i] *  factorial(3)/(factorial(i)*factorial(3-i)) * s ** i * (1-s)**(3-i)
        return h   
        
    def psiDDot(self, q1) :
        ## derivate of psiD in function of q[0,0]
        psiLim = -np.arcsin(self.lStep/(2*self.l))
        s = (q1 +psiLim ) / (2 * psiLim)
        d = (2*psiLim)**2/(self.tStep*3)
        a = [2*psiLim,2*psiLim-d ,-2*psiLim +d,-2*psiLim]
        a = self.param
        h=0
        for i in range (3):
           h += (a[i+1] - a[i]) *  factorial(3)/(factorial(i)*factorial(2-i)) * s ** i * (1-s)**(2-i)
        return h*(1/(2*psiLim))
    
    def psiDDotDot(self, q1):
        ## second derivate of psiD in function of q[0,0]
        psiLim = -np.arcsin(self.lStep/(2*self.l))
        s = (q1 +psiLim ) / (2 * psiLim)
        d = (2*psiLim)**2/(self.tStep*3)
        a = [2*psiLim,2*psiLim-d ,-2*psiLim +d,-2*psiLim]
        a = self.param
        h=0
        for i in range (2):
           h += (a[i+2] -2*a[i+1] + a[i]) *  factorial(3)/(factorial(i)*factorial(1-i)) * s ** i * (1-s)**(1-i)
        return h*(1/(2*psiLim))**2
        
    
    @staticmethod
    def inverse(M):
        if np.linalg.det(M) == 0:
            print('ERROR MASS MATRIX INVERSION -- DEBUGG') #Should never happen according to spectral theorem
            invM = pinv(M) #SVD approx
        else:            
            invM = inv(M)   
        return invM
    
    
    
class Graph() :
    
    def __init__(self, name = '', xlabel = '', ylabel = '', plot1Label = 'Plot 1', plot2Label = 'Plot 2', plot3Label = 'Plot 3', *args, **kwargs):
        ## OPTIONS
        self.plot1Style = kwargs.get('plot1Style') if kwargs.get('plot1Style')!=None else '-'
        self.plot2Style = kwargs.get('plot2Style') if kwargs.get('plot2Style')!=None else '-'
        self.plot3Style = kwargs.get('plot3Style') if kwargs.get('plot3Style')!=None else '-'
        ## WHO AM I
        self.name = name
        ## METADATA
        self.xName = xlabel
        self.yName = ylabel
        self.plot1Name = plot1Label
        self.plot2Name = plot2Label
        self.plot3Name = plot3Label
        ## DATA
        self.x1 = []
        self.y1 = []
        self.x2 = []
        self.y2 = []
        self.x3 = []
        self.y3 = []
        

    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.name
    
    def show(self, save = 0):
        plt.figure()
        plt.plot(self.x1,self.y1,self.plot1Style, label = self.plot1Name)
        if self.x2 != [] :
            plt.plot(self.x2,self.y2,self.plot2Style, label = self.plot2Name)
            plt.legend(prop={'size': 15})
        if self.x3!=[]:
            plt.plot(self.x3,self.y3,self.plot3Style, label = self.plot3Name)
            plt.legend(prop={'size': 15})
        plt.title(self.name)
        plt.xlabel(self.xName, fontsize='15')
        plt.ylabel(self.yName, fontsize='15')
        plt.grid()
        plt.show()
        if save==1:
            plt.savefig(self.name + '.pdf')
        return 1


class Animation() :

    def __init__(self, psi = 0):
        ## WHO AM I
        self.name = '2D animation writer'
        ## ANIMATION DESCRIPTION
        self.psi = psi
        self.nbBody = 0
        self.frames = [[[0,0,0],[0,0,0]],[[0,0,0],[0,0,0]]]
        self.step = 0
    
    def addFrame(self, frame):
        self.frames.append(frame)
    
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.name

    
    def create(self, name = ''):
        ## WHO AM I
        if name != '' :
            self.name = 'Video/' + name
        ## SETUP
        fig = plt.figure()
        ax = fig.add_subplot(111, aspect='equal', autoscale_on=False,
                             xlim=(-2, 16), ylim=(-1, 6))
        ax.grid()
        ax.plot([-2,16], [2*np.tan(self.psi), -16*np.tan(self.psi)], '--r')        
        line, = ax.plot([], [], 'o-', lw=2)
        time_text = ax.text(0.02, 0.95, '', transform=ax.transAxes)
        def animate(i):
            print('%1.2f/100' % (i*100/len(self.frames)))
            line.set_data(self.frames[i])
            time_text.set_text('time : %1.3f s' % (i*self.step) )
            return line, time_text
        def init():
            """initialize animation"""
            line.set_data([], [])
            time_text.set_text('')
            return line, time_text
        matplotlib.use("Agg")
        ## VIDEO CREATION
        ani = animation.FuncAnimation(fig,animate, frames = len(self.frames), blit = True, init_func=init)
        ani.save(self.name + '.mp4', fps=int(1/self.step), extra_args=['-vcodec', 'libx264'])
        matplotlib.use("Qt5Agg")
        return 1
        
simu = Simulation()
simu.launch(7, 0.001, np.array([[0],[0]]),np.array([[0],[0]]), mode = 'euler', control = 'activ')
simu.walker.phasePortrait.show() 
simu.walker.phasePortraitX.show() 
simu.walker.genCoordTimeEvolution.show()
simu.walker.speedGenCoordTimeEvolution.show()
simu.walker.command.show()
simu.walker.obs.show()
simu.walker.obsPsi.show()
simu.walker.energy.show()
simu.walker.poincare.show()
simu.walker.commandPredicted.show()
simu.walker.commandeLin.show()
print(simu.walker.nbContacts)


def testAttractor() :
    q = np.linspace(-0.3,0.3, 100)
    qDot1 = 0
    qDot2 = 0
    a,b,c,d = -1,-1,-1,-1
    attraction = np.zeros((100,100))
    for q1 in q:
        a = (a +1)%100
        for q2 in q:
            b = (b +1)%100
            print(str(a + b*10**(-2)) + '/100')
            simu = Simulation()
            simu.launch(5, 0.001, np.array([[q1],[q2]]),np.array([[qDot1],[qDot2]]), mode = 'euler', control = 'activ')
            attraction[a,b] = simu.walker.nbContacts
    plt.figure()
    q1, q2 = np.meshgrid(q,q)
    plt.contourf(q1,q2,attraction, cmap = 'Reds')
    plt.colorbar()
    plt.xlabel('$q_1$ [rad]')
    plt.ylabel('$q_2$ [rad]')
    plt.show()
    return attraction








