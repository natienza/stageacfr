#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  1 11:41:52 2019

@author: natienza
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 09:24:50 2019

@author: natienza
"""
from scipy.integrate import odeint
import numpy as np
from numpy.linalg import inv
from numpy.linalg import pinv
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.animation as animation


class Simulation() :

    def __init__(self):
        ## SIMULATION PARAMETERS
        self.time = []
        self.step = 0
        self.currentTime = 0
        ## MODEL OF ROBOT
        self.walker = Walker()
        ## WHO AM I
        self.name = 'Simulation of ' + self.walker.name

    
    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name
    
    def launch(self, duration = 30, step = 0.1, q0 = np.zeros((2,1)), qDot0 = np.zeros((2,1)), *args, **kwargs):
        mode = kwargs.get('mode')
        control = kwargs.get('control')
        self.walker.anim.step = step
        self.step = step
        self.time = np.linspace(0,duration,int(duration/step)+1)
        self.walker.setIC(q0, qDot0)
        for t in self.time:
            if t == 0:
                pass
            if mode == 'euler':
                self.walker.simulateEuler(step,t,control)
            elif mode == 'RK' :
                self.walker.simulateRK(step,t,control)
            elif mode == 'scipy':
                self.walker.simulateScipy(step,t,control)
            else :
                print('mode integration non connu')
                return 0
        return
        

class Walker() :
    
    def __init__(self):
        ## WHO AM I
        self.name = '3L Compass gait biped walker'
        ## GEOMETRIC PARAMETERS
        self.a = 1.5
        self.b = 1.5
        self.l = self.a + self.b
        self.mH = 1
        self.m = 1/2
        ## GENERALIZED COORDINATES
        self.q = np.array([ [0],
                            [0]], dtype = np.float64)
        self.qDot = np.array([ [0],
                               [0]], dtype = np.float64)
        self.xq = np.array([0, 0, 0, 0], dtype = np.float64)
        ## STATE SPACE PARAMETERS
        self.x = np.array([ [0],
                            [0],
                            [0],
                            [0]], dtype = np.float64)
        self.u = 0
        self.k = 1
        self.y = 0
        self.v = 0
        ## DYNAMIC SINGLE SUPPORT MODEL
        self.M = np.array([ [self.mH*self.l**2+self.m*self.b**2+self.m*self.l**2, -self.m*self.l*self.a*np.cos(self.x[1,0])],
                            [-self.m*self.l*self.a*np.cos(self.x[1,0]), self.m*self.a**2]])
        
        self.C = np.array([ [0, self.m*self.l*self.a*self.qDot[1,0]*np.sin(self.x[1,0])],
                            [self.m*self.l*self.a*self.qDot[0,0]*np.sin(-self.x[1,0]), 0]])
    
        self.g = np.array([ [-(self.m*self.b + self.m*self.l + self.mH*self.l)*9.81*np.sin(self.x[0,0])],
                            [self.m*self.a*9.81*np.sin(self.q[1,0])]])
        ## IMPACT TRANSITION MATRIX
        self.psi = 0.25
        self.contact = 0 #contact flag
        self.oldq = np.array([ [0],
                            [0]], dtype = np.float64)
        self.eps = 0.001 #epsilon on the contact surface (disrete system)
        self.P = np.array([ [0, 1],
                            [1, 0]])
        self.Pq = np.linalg.solve(np.array([ [self.m*self.a**2 - self.m*self.a*self.l*np.cos(self.x[1,0]), self.m*self.l**2+self.mH*self.l**2+self.m*self.b**2 - self.m*self.a*self.l*np.cos(self.x[1,0])],
                                        [self.m*self.a**2, -self.m*self.a*self.l*np.cos(self.x[1,0])]], dtype = np.float64),np.array([ [(self.m*self.l*self.b + self.mH*self.l**2 + self.m*self.l*self.b)*np.cos(self.x[1,0]), -self.m*self.a*self.b],
                                                                                                                                        [-self.m*self.a*self.b, 0]], dtype = np.float64))
        ## VIRTUAL CONSTRAINTS
        self.lStep = 3
        ## DEBUGG ATTRIBUTES
        self.nbContacts = 0
        self.nbApprox = 0
        self.plotDiff = Graph()
        ## PLOT
        self.phasePortraitX = Graph('Phase portrait', 'q1 [rad]', 'psi [rad]')
        self.phasePortrait = Graph('Phase portrait','q','qDot', '1', '2')
        self.genCoordTimeEvolution = Graph('State time evolution','t','q [rad]' )
        self.speedGenCoordTimeEvolution = Graph('Generalized speed time evolution', 't', 'qDot [rad/s]')
        self.command = Graph('Control input', 't', 'u')
        self.obs = Graph('Observation output', 't', 'y')
        ## ANIMATION
        self.anim = Animation(self.psi)
        self.anim.nbBody = 2
        
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.name

    def getStateVar(self):
        return self.x
    
    def getGenCoord(self):
        return self.q,self.qDot
    
    def f(self,y ,t):
        invM = self.inverse(self.M)
        dq = - np.dot(invM,self.C).dot(self.qDot) - invM.dot(self.g)
        return np.array([[self.qDot[0,0]],
                         [self.qDot[1,0]],
                         [dq[0,0]],
                         [dq[1,0]]])
    
    def setIC(self, q0, qDot0):
        self.q = q0
        self.oldq = q0
        self.qDot = qDot0
        self.xq[0] = q0[0,0]
        self.xq[1] = q0[1,0]
        self.xq[2] = qDot0[0,0]
        self.xq[3] = qDot0[1,0]
        self.x[0,0] = self.q[0,0]
        self.x[1,0] = self.q[1,0] - self.q[0,0]
        self.x[2,0] = self.qDot[0,0]
        self.x[3,0] = self.qDot[1,0] - self.qDot[0,0]
        self.updateDynamicParameters()
        print('IC set')
        return 1 
    
    def updateDynamicParameters(self, *args, **kwargs):
        if len(kwargs) != 0:
            X = kwargs.get('X')
            self.q[0,0] = X[0]
            self.q[1,0] = X[1]
            self.qDot[0,0] = X[2]
            self.qDot[1,0] = X[3]
            self.x[0,0] = self.q[0,0]
            self.x[1,0] = self.q[1,0] - self.q[0,0]
            self.x[2,0] = self.qDot[0,0]
            self.x[3,0] = self.qDot[1,0] - self.qDot[0,0]           
        ## DYNAMIC SINGLE SUPPORT MODEL
        self.M = np.array([ [self.mH*self.l**2+self.m*self.b**2+self.m*self.l**2, -self.m*self.l*self.a*np.cos(self.x[1,0])],
                            [-self.m*self.l*self.a*np.cos(self.x[1,0]), self.m*self.a**2]], dtype = np.float64)
        
        self.C = np.array([ [0, self.m*self.l*self.a*self.qDot[1,0]*np.sin(self.x[1,0])],
                            [self.m*self.l*self.a*self.qDot[0,0]*np.sin(-self.x[1,0]), 0]], dtype = np.float64)
    
        self.g = np.array([ [-(self.m*self.b + self.m*self.l + self.mH*self.l)*9.81*np.sin(self.x[0,0])],
                            [self.m*self.a*9.81*np.sin(self.q[1,0])]], dtype = np.float64)
    
    def updateContactTransition(self):
        ## IMPACT TRANSITION MATRIX
        self.Pq = np.linalg.solve(np.array([ [self.m*self.a**2 - self.m*self.a*self.l*np.cos(self.x[1,0]), self.m*self.l**2+self.mH*self.l**2+self.m*self.b**2 - self.m*self.a*self.l*np.cos(self.x[1,0])],
                                        [self.m*self.a**2, -self.m*self.a*self.l*np.cos(self.x[1,0])]], dtype = np.float64).dot(self.P),np.array([ [(self.m*self.l*self.b + self.mH*self.l**2 + self.m*self.l*self.b)*np.cos(self.x[1,0]), -self.m*self.a*self.b],
                                                                                                                                        [-self.m*self.a*self.b, 0]], dtype = np.float64))

    def surfaceContact(self):
        if (np.cos(self.oldq[0,0]+self.psi) - np.cos(self.oldq[1,0]+self.psi))*(np.cos(self.q[0,0]+self.psi) - np.cos(self.q[1,0]+self.psi)) <= 1e-5 and self.qDot[0,0]<=0:
            if abs(self.q[0,0] - self.q[1,0]) <= 0.2:
                return 0 # false contact : the two end points are colluding
            if  (-self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0])) < 0 :
                return 0  # false contact : we want to go forward
            ## ANIMATION UPDATE
            self.contact = 1
            ## CONTACT TRANSITION MODEL
            self.updateContactTransition()
            self.q = self.P.dot(self.q)
            self.qDot = self.Pq.dot(self.qDot)
            self.xq[0] = self.q[0,0]
            self.xq[1] = self.q[1,0]
            self.xq[2] = self.qDot[0,0]
            self.xq[3] = self.qDot[1,0]
            self.x[0,0] = self.q[0,0] 
            self.x[1,0] = self.q[1,0] - self.q[0,0]
            self.x[2,0] = self.qDot[0,0]
            self.x[3,0] = self.qDot[1,0] - self.qDot[0,0] 
            self.nbContacts += 1
            return 1
        return 0
    
    def updatePlot(self,t):
        self.phasePortrait.x1.append(self.q[0,0])#%(2*np.pi))
        self.phasePortrait.y1.append(self.qDot[0,0])
        self.phasePortrait.x2.append(self.q[1,0])#%(2*np.pi))
        self.phasePortrait.y2.append(self.qDot[1,0])
        self.genCoordTimeEvolution.x1.append(t)
        self.genCoordTimeEvolution.y1.append(self.q[0,0])#%(2*np.pi))
        self.genCoordTimeEvolution.x2.append(t)
        self.genCoordTimeEvolution.y2.append(self.q[1,0])#%(2*np.pi))
        self.speedGenCoordTimeEvolution.x1.append(t)
        self.speedGenCoordTimeEvolution.y1.append(self.qDot[0,0])
        self.speedGenCoordTimeEvolution.x2.append(t)
        self.speedGenCoordTimeEvolution.y2.append(self.qDot[1,0])
        self.plotDiff.x1.append(t)
        self.plotDiff.y1.append((np.cos(self.oldq[0,0]) - np.cos(self.oldq[1,0]))*(np.cos(self.q[0,0]) - np.cos(self.q[1,0])))
        self.phasePortraitX.x1.append(self.x[0,0])
        self.phasePortraitX.y1.append(self.x[1,0])
        self.command.x1.append(t)
        self.command.y1.append(self.u)
        self.obs.x1.append(t)
        self.obs.y1.append(self.x[0,0] - self.psiD())
        ## UPDATE ANIMATION
        self.updateAnimation()
        return         
    
    def simulateEuler(self, step, t, mode='passiv'):
        if mode == 'passiv':
            b = 0
        else :
            b = 1
        ## SURFACE DE CONTACT
        self.surfaceContact()
        ## CONTROL
        self.controlFeedback()
        ## EULER INTEGRATION    
        qNew = self.q + step*self.qDot
        if np.linalg.det(self.M) == 0:
            print('ERROR MASS MATRIX INVERSION -- DEBUGG') #Should never happen according to spectral theorem
            invM = pinv(self.M) #SVD approx
            self.nbApprox += 1
        else:            
            invM = inv(self.M)
        qDotNew = self.qDot - step*( np.dot(invM,self.C).dot(self.qDot) + invM.dot(self.g))
        ## DYNAMIC MODEL EVOLUTION
        self.oldq = self.q
        self.q = qNew 
        self.qDot = qDotNew
        self.x[0,0] = self.q[0,0] 
        self.x[1,0] = self.q[1,0] - self.q[0,0]
        self.x[2,0] = self.qDot[0,0]
        self.x[3,0] = self.qDot[1,0] - self.qDot[0,0] + b*self.u
        self.updateDynamicParameters()
        ## UPDATE PLOT
        self.updatePlot(t)
        return 1

    def simulateRK(self, step, t, mode='passiv'):
        if mode == 'passiv':
            b = 0
        else :
            b = 1
        ## SURFACE DE CONTACT
        self.surfaceContact()
        ## CONTROL
        self.controlFeedback()
        if np.linalg.det(self.M) == 0:
            print('ERROR MASS MATRIX INVERSION -- DEBUGG') #Should never happen according to spectral theorem
            invM = pinv(self.M)#SVD approx
            self.nbApprox += 1
        else:            
            invM = inv(self.M)    
        ## DYNAMIC MODEL
        self.oldq = self.q.copy()
        def f(X):
            dq = np.array([[X[2,0]],
                           [X[3,0]]], dtype = np.float64)
            dq += -invM.dot(self.C.dot(dq) + self.g)
            return np.array([[X[2,0]],
                             [X[3,0]],
                             [dq[0,0]],
                             [dq[1,0]]], dtype = np.float64)
        ## RUNGE-KUTTA INTEGRATION (order 4)
        Xq = np.array([[self.q[0,0]],
                       [self.q[1,0]],
                       [self.qDot[0,0]],
                       [self.qDot[1,0]]], dtype = np.float64)
        k1 = f(Xq)
        k2 = f(Xq+step*k1/2)
        k3 = f(Xq+step*k2/2)
        k4 = f(Xq + step*k3)
        Xq = Xq + step/6 * (k1 + 2*k2 + 2*k3 + k4)
        ## UPDATING
        self.q[0,0] = Xq[0,0]
        self.q[1,0] = Xq[1,0]
        self.qDot[0,0] = Xq[2,0]
        self.qDot[1,0] = Xq[3,0]
        self.x[0,0] = self.q[0,0] 
        self.x[1,0] = self.q[1,0] - self.q[0,0]
        self.x[2,0] = self.qDot[0,0]
        self.x[3,0] = self.qDot[1,0] - self.qDot[0,0] + b*self.u
        self.updateDynamicParameters()
        ## UPDATE PLOT
        self.updatePlot(t)
        return 1
    
    def fsp(self,X,t):
        self.updateDynamicParameters(X = X)
        invM = self.inverse(self.M)
        dq = np.array([[X[2]],
                      [X[3]]])
        dq = -invM.dot(self.C.dot(dq) + self.g)
#        if self.b == 1 :    
#            self.u = -(dq[1,0] - dq[0,0]) - self.k * (X[1] - X[0]) + self.psiDDot()*dq[0,0] + self.psiDDotDot() * X[2]
#            dq += np.array([[-self.u], [self.u]])
        return np.array([X[2], X[3], dq[0,0], dq[1,0]])
        
    
    def simulateScipy(self,step, t, mode = 'passiv'):
        if mode == 'passiv':
            b = 0
        else :
            b = 1
        ## CONTACT MAP
        self.surfaceContact()
        ## INTEGRATION
        integ = odeint(self.fsp, self.xq, np.array([t-step, t]))
        self.xq = integ[-1,:]
        ## UPDATING
        self.oldq = self.q.copy()
        self.q[0,0] = self.xq[0]
        self.q[1,0] = self.xq[1]
        self.qDot[0,0] = self.xq[2]
        self.qDot[1,0] = self.xq[3]
        self.x[0,0] = self.q[0,0] 
        self.x[1,0] = self.q[1,0] - self.q[0,0]
        self.x[2,0] = self.qDot[0,0]
        self.x[3,0] = self.qDot[1,0] - self.qDot[0,0]
        self.updateDynamicParameters()
        ## UPDATE PLOT
        self.updatePlot(t)
        return 1
    
    
    def updateAnimation(self):
        lastRef = [self.anim.frames[-1][0][0],self.anim.frames[-1][1][0]]
        if self.contact == 1:
            self.contact = 0
            lastRef[0] -= -self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0])  
            lastRef[1] -= self.l*(np.cos(self.q[0,0]) - np.cos(self.q[1,0]))
            print(lastRef)
        p1 = [-self.l*np.sin(self.q[0,0])+ lastRef[0], self.l*np.cos(self.q[0,0])+ lastRef[1]]
        p2 = [-self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0]) + lastRef[0], self.l*np.cos(self.q[0,0]) - self.l*np.cos(self.q[1,0]) + lastRef[1]]
        self.anim.frames.append([[lastRef[0],p1[0],p2[0]],[lastRef[1],p1[1],p2[1]]])           
        return 1
    
    def psiD(self) :
        psiLim = np.arcsin(self.lStep/(2*self.l))
        s = (self.q[0,0] +psiLim ) / (2 * psiLim)
        a = [-psiLim,-psiLim,psiLim,psiLim]
        h=0
        for i in range (4):
           h += a[i] *  self.fact(3)/(self.fact(i)*self.fact(3-i)) * s ** i * (1-s)**(3-i)
        return h   
        
    def psiDDot(self) :
        ## derivate of psiD in function of q[0,0]
        psiLim = np.arcsin(self.lStep/(2*self.l))
        s = (self.q[0,0] +psiLim ) / (2 * psiLim)
        a = [-psiLim,-psiLim,psiLim,psiLim]
        h=0
        for i in range (3):
           h += (a[i+1] - a[i]) *  self.fact(3)/(self.fact(i)*self.fact(2-i)) * s ** i * (1-s)**(2-i)
        return h*(psiLim/(2*psiLim))
    
    def psiDDotDot(self):
        ## second derivate of psiD in function of q[0,0]
        psiLim = np.arcsin(self.lStep/(2*self.l))
        s = (self.q[0,0] +psiLim ) / (2 * psiLim)
        a = [-psiLim,-psiLim,psiLim,psiLim]
        h=0
        for i in range (2):
           h += (a[i+2] -2*a[i+1] + a[i]) *  self.fact(3)/(self.fact(i)*self.fact(1-i)) * s ** i * (1-s)**(1-i)
        return h*(psiLim/(2*psiLim))**2
        
    
    @staticmethod
    def inverse(M):
        if np.linalg.det(M) == 0:
            print('ERROR MASS MATRIX INVERSION -- DEBUGG') #Should never happen according to spectral theorem
            invM = pinv(M) #SVD approx
        else:            
            invM = inv(M)   
        return invM

    def fact(self,n):
        if n==0:
            return 1
        return n*self.fact(n-1)
    
class Graph() :
    
    def __init__(self, name = '', xlabel = '', ylabel = '', plot1Label = 'Plot 1', plot2Label = 'Plot 2'):
        ## WHO AM I
        self.name = name
        ## METADATA
        self.xName = xlabel
        self.yName = ylabel
        self.plot1Name = plot1Label
        self.plot2Name = plot2Label
        ## DATA
        self.x1 = []
        self.y1 = []
        self.x2 = []
        self.y2 = []
        

    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.name
    
    def show(self, save = 0):
        plt.figure()
        plt.plot(self.x1,self.y1, label = self.plot1Name)
        if self.x2 != [] :
            plt.plot(self.x2,self.y2, label = self.plot2Name)
            plt.legend()
        plt.title(self.name)
        plt.xlabel(self.xName)
        plt.ylabel(self.yName)
        plt.grid()
        plt.show()
        if save==1:
            plt.savefig(self.name + '.pdf')
        return 1


class Animation() :

    def __init__(self, psi = 0):
        ## WHO AM I
        self.name = '2D animation writer'
        ## ANIMATION DESCRIPTION
        self.psi = psi
        self.nbBody = 0
        self.frames = [[[0,0,0],[0,0,0]],[[0,0,0],[0,0,0]]]
        self.step = 0
    
    def addFrame(self, frame):
        self.frames.append(frame)
    
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.name

    
    def create(self, name = ''):
        ## WHO AM I
        if name != '' :
            self.name = 'Video/' + name
        ## SETUP
        fig = plt.figure()
        ax = fig.add_subplot(111, aspect='equal', autoscale_on=False,
                             xlim=(-2, 16), ylim=(-5, 5))
        ax.grid()
        ax.plot([-2,16], [2*np.tan(self.psi), -16*np.tan(self.psi)], '--r')        
        line, = ax.plot([], [], 'o-', lw=2)
        time_text = ax.text(0.02, 0.95, '', transform=ax.transAxes)
        def animate(i):
            print('%1.3 ' % i*100/len(self.frames))
            line.set_data(self.frames[i])
            time_text.set_text('time : %1.3f s' % (i*self.step) )
            return line, time_text
        def init():
            """initialize animation"""
            line.set_data([], [])
            time_text.set_text('')
            return line, time_text
        matplotlib.use("Agg")
        ## VIDEO CREATION
        ani = animation.FuncAnimation(fig,animate, frames = len(self.frames), blit = True, init_func=init)
        ani.save(self.name + '.mp4', fps=int(1/self.step), extra_args=['-vcodec', 'libx264'])
        matplotlib.use("Qt5Agg")
        return 1
        
simu = Simulation()
simu.launch(5, 0.001, np.array([[0.01],[-1.3]]),np.array([[-0.254],[0.05]]), mode = 'scipy', control = 'passiv')
simu.walker.phasePortrait.show() 
simu.walker.phasePortraitX.show() 
simu.walker.genCoordTimeEvolution.show()
simu.walker.speedGenCoordTimeEvolution.show()
simu.walker.command.show()
simu.walker.obs.show()
print(simu.walker.nbContacts)




