#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 09:24:50 2019

@author: natienza
"""

from scipy.integrate import odeint
import numpy as np
from numpy.linalg import inv
from numpy.linalg import pinv
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.animation as animation
from math import factorial
import random as rd

def sgn(x):
    if x==0:
        return 1
    else:
        return x/abs(x)

class Simulation() :

    def __init__(self):
        ## SIMULATION PARAMETERS
        self.time = []
        self.step = 0
        self.currentTime = 0
        ## MODEL OF ROBOT
        self.walker = Walker()
        ## CONTROL HIGH POLICY AGENT
        self.agent = Agent()
        ## WHO AM I
        self.name = 'Simulation of ' + self.walker.name

    
    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name
    
    def launch(self, duration = 30, step = 0.1, q0 = np.zeros((2,1)), qDot0 = np.zeros((2,1)), *args, **kwargs):
        mode = kwargs.get('mode')
        control = kwargs.get('control')
        controlMode = kwargs.get('controlMode') if kwargs.get('controlMode')!=None else 'PD'
        self.walker.anim.step = step
        self.step = step
        self.time = np.linspace(0,duration,int(duration/step)+1)
        self.walker.setIC(q0, qDot0)
        self.walker.controlMode = controlMode
        #self.walker.consigne = self.agent.choose([self.walker.q,self.walker.qDot])
        for t in self.time:
            if t == 0:
                pass
            if mode == 'euler':
                if not(self.walker.simulateEuler(step,t,control)):
                    return 0
                if self.walker.poincareImpact():
                    pass
                    print(simu.walker.consigne)
                    self.walker.consigne = self.agent.choose([self.walker.q,self.walker.qDot])
                    print(simu.walker.consigne)
                    print('\n')
            elif mode == 'RK' :
                self.walker.simulateRK(step,t,control)
            elif mode == 'scipy':
                self.walker.simulateScipy(step,t,control)
            else :
                print('mode integration non connu')
                return 0
        return
        

class Walker() :
    
    def __init__(self, *args, **kwargs):
        ## WHO AM I
        self.name = '3L Compass gait biped walker'
        ## GEOMETRIC PARAMETERS
        self.a = 1.5
        self.b = 1.5
        self.l = self.a + self.b
        self.mH = 5.07#2
        self.m = 5#3/4
        ## GENERALIZED COORDINATES
        self.q = np.array([ [0],
                            [0]], dtype = np.float64)
        self.qDot = np.array([ [0],
                               [0]], dtype = np.float64)
        self.xq = np.array([0, 0, 0, 0], dtype = np.float64)
        ## STATE SPACE PARAMETERS
        self.x = np.array([ 0,0,0,0], dtype = np.float64)
        self.u = np.array([0],dtype = np.float64)
        self.a1 = 25
        self.a0 = 10
        self.y = 0
        self.v = 0
        ## CONTROL
        self.controlMode = kwargs.get('controlMode') if kwargs.get('controlMode')!=None else 'PD'
        ## ENERGY
        self.k = 0
        self.V = 0
        ## DYNAMIC SINGLE SUPPORT MODEL
        self.M = np.array([ [self.mH*self.l**2+self.m*self.b**2+self.m*self.l**2, -self.m*self.l*self.a*np.cos(self.x[1])],
                            [-self.m*self.l*self.a*np.cos(self.x[1]), self.m*self.a**2]])
        
        self.C = np.array([ [0, self.m*self.l*self.a*self.qDot[1,0]*np.sin(self.x[1])],
                            [self.m*self.l*self.a*self.qDot[0,0]*np.sin(-self.x[1]), 0]])
    
        self.g = np.array([ [-(self.m*self.b + self.m*self.l + self.mH*self.l)*9.81*np.sin(self.x[0])],
                            [self.m*self.a*9.81*np.sin(self.q[1,0])]])
        ## IMPACT TRANSITION MATRIX
        self.psi = 0.07
        self.contact = 0 #contact flag
        self.oldq = np.array([ [0],
                            [0]], dtype = np.float64)
        self.eps = 0.001 #epsilon on the contact surface (disrete system)
        self.P = np.array([ [0, 1],
                            [1, 0]])
        self.Pq = np.linalg.solve(np.array([ [self.m*self.a**2 - self.m*self.a*self.l*np.cos(self.x[1]), self.m*self.l**2+self.mH*self.l**2+self.m*self.b**2 - self.m*self.a*self.l*np.cos(self.x[1])],
                                        [self.m*self.a**2, -self.m*self.a*self.l*np.cos(self.x[1])]], dtype = np.float64),np.array([ [(self.m*self.l*self.b + self.mH*self.l**2 + self.m*self.l*self.b)*np.cos(self.x[1]), -self.m*self.a*self.b],
                                                                                                                                        [-self.m*self.a*self.b, 0]], dtype = np.float64))
        ## VIRTUAL CONSTRAINTS
        self.lStep = 2
        self.tStep = 0.2
        self.consigne = 0.7254297928385293 #np.arcsin(self.lStep/(2*self.l))
        ## DEBUGG ATTRIBUTES
        self.nbContacts = 0
        self.nbApprox = 0
        self.plotDiff = Graph()
        ## PLOT
        self.phasePortraitX = Graph('$\psi$ versus $q_1$', '$q_1$ [rad]', '$\psi$ [rad]')
        self.phasePortrait = Graph('Phase portrait','$q$ [rad]','$\dot{q}$ [rad/s]', '1', '2')
        self.genCoordTimeEvolution = Graph('State time evolution','t [s]','q [rad]' )
        self.speedGenCoordTimeEvolution = Graph('Generalized speed versus time', 't [s]', '$\dot{q}$ [rad/s]')
        self.command = Graph('Control input (hip torque versus time)', 't [s]', 'u [Nm]')
        self.obs = Graph('Observation output', 't [s]', 'y [rad]')
        self.obsPsi = Graph('$\psi$ versus time', 't [s]', '$\psi$ [rad]')
        self.commandeLin = Graph('linear command input', 't [s]', 'v [Nm]')
        self.energy = Graph('Energy versus time', 't [s]', 'Energy [J]', '$K$', '$V$', '$V-K$', plot3Style = '--')
        self.poincare = Graph('Poincare map', '$q_1$ [rad]', '$q_2 [rad]$', plot1Style = '--o')
        ## ANIMATION
        self.anim = Animation(self.psi)
        self.anim.nbBody = 2
        
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.name

    def getStateVar(self):
        return self.x
    
    def getGenCoord(self):
        return self.q,self.qDot
    
    def f(self,y ,t):
        invM = self.inverse(self.M)
        dq = - np.dot(invM,self.C).dot(self.qDot) - invM.dot(self.g)
        return np.array([[self.qDot[0,0]],
                         [self.qDot[1,0]],
                         [dq[0,0]],
                         [dq[1,0]]])
    
    def setIC(self, q0, qDot0):
        self.q = q0
        self.oldq = q0
        self.qDot = qDot0
        self.xq[0] = q0[0,0]
        self.xq[1] = q0[1,0]
        self.xq[2] = qDot0[0,0]
        self.xq[3] = qDot0[1,0]
        self.x[0] = self.q[0,0]
        self.x[1] = self.q[1,0] - self.q[0,0]
        self.x[2] = self.qDot[0,0]
        self.x[3] = self.qDot[1,0] - self.qDot[0,0]
        self.updateDynamicParameters()
        #print('IC set')
        return 1 
    
    def updateDynamicParameters(self, *args, **kwargs):
        if len(kwargs) != 0:
            X = kwargs.get('X')
            ''' X = np.array([q1,psi,q1Dot, psiDot])'''
            self.x = X
            self.xq = np.array([self.x[0], self.x[1]+self.x[0], self.x[2], self.x[2]+self.x[3]])
            self.q = np.array([[self.xq[0]],[self.xq[1]]])
            self.qDot = np.array([[self.xq[2]], [self.xq[3]]])
        ## DYNAMIC SINGLE SUPPORT MODEL
        self.M = np.array([ [self.mH*self.l**2+self.m*self.b**2+self.m*self.l**2, -self.m*self.l*self.a*np.cos(self.x[1])],
                            [-self.m*self.l*self.a*np.cos(self.x[1]), self.m*self.a**2]], dtype = np.float64)
        
        self.C = np.array([ [0, self.m*self.l*self.a*self.qDot[1,0]*np.sin(self.x[1])],
                            [self.m*self.l*self.a*self.qDot[0,0]*np.sin(-self.x[1]), 0]], dtype = np.float64)
    
        self.g = np.array([ [-(self.m*self.b + self.m*self.l + self.mH*self.l)*9.81*np.sin(self.x[0])],
                            [self.m*self.a*9.81*np.sin(self.q[1,0])]], dtype = np.float64)
        ## ENERGY
        self.k = 0.5 * self.m * ( self.b * self.x[2]**2 + (self.l*self.x[2]*np.sin(self.x[0]) + self.a*self.qDot[1,0]*np.sin(self.q[1,0]) )**2 + (self.l*self.x[2]*np.cos(self.x[0]) + self.a*self.qDot[1,0]*np.cos(self.q[1,0])) **2  ) + 0.5*self.mH*self.qDot[0,0]**2
        self.V =0.5 * self.m * (self.b + self.l) * np.cos(self.x[0]) + 0.5 * self.mH * (self.l*np.cos(self.x[0]) - self.a*np.cos(self.q[1,0]))
    
    def updateContactTransition(self):
        ## IMPACT TRANSITION MATRIX
        self.Pq = np.linalg.solve(np.array([ [self.m*self.a**2 - self.m*self.a*self.l*np.cos(self.x[1]), self.m*self.l**2+self.mH*self.l**2+self.m*self.b**2 - self.m*self.a*self.l*np.cos(self.x[1])],
                                        [self.m*self.a**2, -self.m*self.a*self.l*np.cos(self.x[1])]], dtype = np.float64).dot(self.P),np.array([ [(self.m*self.l*self.b + self.mH*self.l**2 + self.m*self.l*self.b)*np.cos(self.x[1]), -self.m*self.a*self.b],
                                                                                                                                        [-self.m*self.a*self.b, 0]], dtype = np.float64))

    def surfaceContact(self):
        if (np.cos(self.oldq[0,0]+self.psi) - np.cos(self.oldq[1,0]+self.psi))*(np.cos(self.q[0,0]+self.psi) - np.cos(self.q[1,0]+self.psi)) <= 1e-5 and self.qDot[0,0]<=0:
            if abs(self.q[0,0] - self.q[1,0]) <= 0.2:
                return 0 # false contact : the two end points are colluding
            if  (-self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0])) < 0 :
                return 0  # false contact : we want to go forward
            if abs(-self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0])) < 0.3:
                return 0 # false contact
            ## DEBUGG
            print('\nCONTACT\n')
            ## POINCARE MAP UPDATE
            self.poincare.x1.append(self.x[0])
            self.poincare.y1.append(self.x[1])
            ## ANIMATION UPDATE
            self.contact = 1
            ## CONTACT TRANSITION MODEL
            self.updateContactTransition()
            self.q = self.P.dot(self.q)
            self.qDot = self.Pq.dot(self.qDot)
            self.xq = np.array([self.q[0,0], self.q[1,0], self.qDot[0,0], self.qDot[1,0]])
            self.x = np.array([self.q[0,0],self.q[1,0]-self.q[0,0], self.qDot[0,0], self.qDot[1,0]-self.qDot[0,0]])
            self.nbContacts += 1
            return 1
        return 0
    
    def poincareImpact(self):
        if (self.oldq[1,0] - self.oldq[0,0])*(self.q[1,0] - self.q[0,0])<0:
            return True
        return False
    
    def updatePlot(self,t):
        self.phasePortrait.x1.append(self.q[0,0])#%(2*np.pi))
        self.phasePortrait.y1.append(self.qDot[0,0])
        self.phasePortrait.x2.append(self.q[1,0])#%(2*np.pi))
        self.phasePortrait.y2.append(self.qDot[1,0])
        self.genCoordTimeEvolution.x1.append(t)
        self.genCoordTimeEvolution.y1.append(self.q[0,0])#%(2*np.pi))
        self.genCoordTimeEvolution.x2.append(t)
        self.genCoordTimeEvolution.y2.append(self.q[1,0])#%(2*np.pi))
        self.speedGenCoordTimeEvolution.x1.append(t)
        self.speedGenCoordTimeEvolution.y1.append(self.qDot[0,0])
        self.speedGenCoordTimeEvolution.x2.append(t)
        self.speedGenCoordTimeEvolution.y2.append(self.qDot[1,0])
        self.plotDiff.x1.append(t)
        self.plotDiff.y1.append((np.cos(self.oldq[0,0]) - np.cos(self.oldq[1,0]))*(np.cos(self.q[0,0]) - np.cos(self.q[1,0])))
        self.phasePortraitX.x1.append(self.x[0])
        self.phasePortraitX.y1.append(self.x[1])
        self.command.x1.append(t)
        self.command.y1.append(self.u[0])
        self.obs.x1.append(t)
        self.obs.y1.append(self.x[1] - self.psiD(self.x[0]))
        self.obsPsi.x1.append(t)
        self.obsPsi.y1.append(self.x[1])
        self.commandeLin.x1.append(t)
        self.commandeLin.y1.append(self.v)
        self.energy.x1.append(t)
        self.energy.y1.append(self.k)
        self.energy.x2.append(t)
        self.energy.y2.append(self.V)
        self.energy.x3.append(t)
        self.energy.y3.append(-self.k+self.V)
        ## UPDATE ANIMATION
        self.updateAnimation(t)
        return         
    
    def fsp2(self, X, t):
        ''' X = np.array([q1,psi,q1Dot, psiDot])'''
        self.updateDynamicParameters(X=X)
        dq = np.array([[X[2]],
                       [X[2]+X[3]]])
        invM = self.inverse(self.M)
        ddq = -invM.dot(self.C.dot(dq) + self.g)
        ddx = np.array([ddq[0,0], ddq[1,0]-ddq[0,0]])
        if self.control and (t/0.001)%5==0:
            if self.controlMode == 'feedbackLinearisation':
                self.v = -self.a0 * (X[3] - self.psiDDot(X[0])*X[2]) - self.a1*(X[1]-self.psiD(X[0]))
                self.u[0] = -ddx[1] + self.v + self.psiDDotDot(X[0])*X[2]**2 + self.psiDDot(X[0])*ddx[0]
            elif self.controlMode == 'PD':
                self.u[0] = -100 * (self.x[1] - np.arcsin(self.lStep/(2*self.l))) - 10*self.x[3]
            ddx = np.array([ddx[0], ddx[1] + self.u[0]])
        return np.array([X[2], X[3], ddx[0], ddx[1]])
    
    def simulateEuler(self, step, t, mode='passiv'):
        for i in self.x:
            if i>10:
                return 0
        if mode == 'passiv':
            self.control = False
        else :
            self.control = True
        ## SURFACE DE CONTACT
        self.surfaceContact()
        ## EULER INTEGRATION
        self.x =  (self.x + step*self.fsp2(self.x,t))
        self.xq = np.array([self.x[0], self.x[1]+self.x[0], self.x[2], self.x[2]+self.x[3]])
        ## DYNAMIC MODEL EVOLUTION
        self.oldq = self.q.copy()
        self.q = np.array([[self.xq[0]],[self.xq[1]]])
        self.qDot = np.array([[self.xq[2]], [self.xq[3]]])
        self.updateDynamicParameters()
        ## UPDATE PLOT
        self.updatePlot(t)
        return 1

    def simulateRK(self, step, t, mode='passiv'):
        if mode == 'passiv':
            self.control = False
        else :
            self.control = True
        ## SURFACE DE CONTACT
        self.surfaceContact()
        ## CONTROL
        self.controlFeedback()
        if np.linalg.det(self.M) == 0:
            print('ERROR MASS MATRIX INVERSION -- DEBUGG') #Should never happen according to spectral theorem
            invM = pinv(self.M)#SVD approx
            self.nbApprox += 1
        else:            
            invM = inv(self.M)    
        ## DYNAMIC MODEL
        self.oldq = self.q.copy()
        def f(X):
            dq = np.array([[X[2,0]],
                           [X[3,0]]], dtype = np.float64)
            dq += -invM.dot(self.C.dot(dq) + self.g)
            return np.array([[X[2,0]],
                             [X[3,0]],
                             [dq[0,0]],
                             [dq[1,0]]], dtype = np.float64)
        ## RUNGE-KUTTA INTEGRATION (order 4)
        Xq = np.array([[self.q[0,0]],
                       [self.q[1,0]],
                       [self.qDot[0,0]],
                       [self.qDot[1,0]]], dtype = np.float64)
        k1 = f(Xq)
        k2 = f(Xq+step*k1/2)
        k3 = f(Xq+step*k2/2)
        k4 = f(Xq + step*k3)
        Xq = Xq + step/6 * (k1 + 2*k2 + 2*k3 + k4)
        ## UPDATING
        self.q[0,0] = Xq[0,0]
        self.q[1,0] = Xq[1,0]
        self.qDot[0,0] = Xq[2,0]
        self.qDot[1,0] = Xq[3,0]
        self.x[0,0] = self.q[0,0] 
        self.x[1,0] = self.q[1,0] - self.q[0,0]
        self.x[2,0] = self.qDot[0,0]
        self.x[3,0] = self.qDot[1,0] - self.qDot[0,0] + b*self.u
        self.updateDynamicParameters()
        ## UPDATE PLOT
        self.updatePlot(t)
        return 1
    
    def fsp(self,X,t):
        ''' X = np.array([q1, q2, q1Dot, q2Dot])'''
        self.updateDynamicParameters(X = X)
        invM = self.inverse(self.M)
        dq = np.array([[X[2]],
                      [X[3]]])
        dq = -invM.dot(self.C.dot(dq) + self.g).copy()
        if self.control :
            self.v = -self.a0 * (X[3] - X[2] - self.psiDDot(X[0])*X[2]) - self.a1 * (X[1]-X[0] - self.psiD(X[0]))
            self.u = np.array([ -(dq[1,0] - dq[0,0]) + self.v + self.psiDDot(X[0])*dq[0,0] + self.psiDDotDot(X[0]) * X[2]**2])
            dq += np.array([[-self.u[0]], [self.u[0]]])
        return np.array([X[2], X[3], dq[0,0], dq[1,0]])
        
    
    def simulateScipy(self,step, t, mode = 'passiv'):
        if mode == 'passiv':
            self.control = False
        else :
            self.control = True
        ## CONTACT MAP
        self.surfaceContact()
        ## INTEGRATION
        integ = odeint(self.fsp2, self.x, np.array([t-step, t]))
        self.x = integ[-1,:].copy()
        ## UPDATING
        self.xq = np.array([self.x[0], self.x[1]+self.x[0], self.x[2], self.x[2]+self.x[3]])
        ## DYNAMIC MODEL EVOLUTION
        self.oldq = self.q.copy()
        self.q = np.array([[self.xq[0]],[self.xq[1]]])
        self.qDot = np.array([[self.xq[2]], [self.xq[3]]])
        self.updateDynamicParameters()
        ## UPDATE PLOT
        self.updatePlot(t)
        return 1
    
    
    def updateAnimation(self, t=0):
        lastRef = [self.anim.frames[-1][0][0],self.anim.frames[-1][1][0]]
        if self.contact == 1:
            self.contact = 0
            lastRef[0] -= -self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0])  
            lastRef[1] -= self.l*(np.cos(self.q[0,0]) - np.cos(self.q[1,0]))
            #print(str(lastRef) + ' -> ' + str(t) )
        p1 = [-self.l*np.sin(self.q[0,0])+ lastRef[0], self.l*np.cos(self.q[0,0])+ lastRef[1]]
        p2 = [-self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0]) + lastRef[0], self.l*np.cos(self.q[0,0]) - self.l*np.cos(self.q[1,0]) + lastRef[1]]
        self.anim.frames.append([[lastRef[0],p1[0],p2[0]],[lastRef[1],p1[1],p2[1]]])           
        return 1
    
    def psiD(self, q1) :
        psiLim = -np.arcsin(self.lStep/(2*self.l))
        s = (q1 +psiLim ) / (2 * psiLim)
        d = (2*psiLim)**2/(self.tStep*3)
        a = [2*psiLim,2*psiLim-d ,-2*psiLim +d,-2*psiLim]
        h=0
        for i in range (4):
           h += a[i] *  factorial(3)/(factorial(i)*factorial(3-i)) * s ** i * (1-s)**(3-i)
        return h   
        
    def psiDDot(self, q1) :
        ## derivate of psiD in function of q[0,0]
        psiLim = -np.arcsin(self.lStep/(2*self.l))
        s = (q1 +psiLim ) / (2 * psiLim)
        d = (2*psiLim)**2/(self.tStep*3)
        a = [2*psiLim,2*psiLim-d ,-2*psiLim +d,-2*psiLim]
        h=0
        for i in range (3):
           h += (a[i+1] - a[i]) *  factorial(3)/(factorial(i)*factorial(2-i)) * s ** i * (1-s)**(2-i)
        return h*(1/(2*psiLim))
    
    def psiDDotDot(self, q1):
        ## second derivate of psiD in function of q[0,0]
        psiLim = -np.arcsin(self.lStep/(2*self.l))
        s = (q1 +psiLim ) / (2 * psiLim)
        d = (2*psiLim)**2/(self.tStep*3)
        a = [2*psiLim,2*psiLim-d ,-2*psiLim +d,-2*psiLim]
        h=0
        for i in range (2):
           h += (a[i+2] -2*a[i+1] + a[i]) *  factorial(3)/(factorial(i)*factorial(1-i)) * s ** i * (1-s)**(1-i)
        return h*(1/(2*psiLim))**2
        
    
    @staticmethod
    def inverse(M):
        if np.linalg.det(M) == 0:
            print('ERROR MASS MATRIX INVERSION -- DEBUGG') #Should never happen according to spectral theorem
            invM = pinv(M) #SVD approx
        else:            
            invM = inv(M)   
        return invM   


class Agent(Walker): 
    
    def __init__(self):
        super(Agent, self).__init__()
        ## WHO AM I 
        self.name = 'Agent'
        ## PARAMETERS
        self.step = 0.01
        self.actionValue = {}
        self.epsilon = 0.01
        self.gamma = 0.99
        self.nActions = 20
        ## INITIALIZE
        self.controlMode = 'PD'
        self.createSetAction()
        
    def __repr__(self):
        return self.name
    
    def __str__(self):
        return self.name
    
    def createSetAction(self):
        for i in range (self.nActions):
            self.actionValue[rd.random()] = 0
    
    @staticmethod
    def argMaxAction(dico):
        listQ = list(dico.values())
        maxQ = max(listQ)
        count = 0
        index = []
        for i in range(len(listQ)):
            q = listQ[i]
            if q == maxQ:
                count += 1
                index += [i]
        if count > 1:
            return list(dico.keys())[rd.sample(index,1)[0]]
        return list(dico.keys())[np.argmax(list(dico.values()))]
    
    def act(self, state, action):
        self.actionValue[action] = 0 if self.actionValue.get(action) == None else self.actionValue[action] #for debugg
        self.setIC(state[0], state[1])
        for t in np.arange(0,2,self.step):
            self.simulateEuler(self.step, t, 'activ')
            if self.poincareImpact():
                self.actionValue[action] += 1
                return 1
        self.actionValue[action] += -1
        return -1
    
    def choose(self, state):
        action = self.argMaxAction(self.actionValue)
        cpt = 0
        while self.act(state, action) < 0:
            action = self.argMaxAction(self.actionValue)
            cpt += 1
            if cpt >= 30:
                print('ERROR')
                break
        return action
    
class Graph() :
    
    def __init__(self, name = '', xlabel = '', ylabel = '', plot1Label = 'Plot 1', plot2Label = 'Plot 2', plot3Label = 'Plot 3', *args, **kwargs):
        ## OPTIONS
        self.plot1Style = kwargs.get('plot1Style') if kwargs.get('plot1Style')!=None else '-'
        self.plot2Style = kwargs.get('plot2Style') if kwargs.get('plot2Style')!=None else '-'
        self.plot3Style = kwargs.get('plot3Style') if kwargs.get('plot3Style')!=None else '-'
        ## WHO AM I
        self.name = name
        ## METADATA
        self.xName = xlabel
        self.yName = ylabel
        self.plot1Name = plot1Label
        self.plot2Name = plot2Label
        self.plot3Name = plot3Label
        ## DATA
        self.x1 = []
        self.y1 = []
        self.x2 = []
        self.y2 = []
        self.x3 = []
        self.y3 = []
        

    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.name
    
    def show(self, save = 0):
        plt.figure()
        plt.plot(self.x1,self.y1,self.plot1Style, label = self.plot1Name)
        if self.x2 != [] :
            plt.plot(self.x2,self.y2,self.plot2Style, label = self.plot2Name)
            plt.legend()
        if self.x3!=[]:
            plt.plot(self.x3,self.y3,self.plot3Style, label = self.plot3Name)
            plt.legend()
        plt.title(self.name)
        plt.xlabel(self.xName)
        plt.ylabel(self.yName)
        plt.grid()
        plt.show()
        if save==1:
            plt.savefig(self.name + '.pdf')
        return 1


class Animation() :

    def __init__(self, psi = 0):
        ## WHO AM I
        self.name = '2D animation writer'
        ## ANIMATION DESCRIPTION
        self.psi = psi
        self.nbBody = 0
        self.frames = [[[0,0,0],[0,0,0]],[[0,0,0],[0,0,0]]]
        self.step = 0
    
    def addFrame(self, frame):
        self.frames.append(frame)
    
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.name

    
    def create(self, name = ''):
        ## WHO AM I
        if name != '' :
            self.name = 'Video/' + name
        ## SETUP
        fig = plt.figure()
        ax = fig.add_subplot(111, aspect='equal', autoscale_on=False,
                             xlim=(-2, 16), ylim=(-6, 6))
        ax.grid()
        ax.plot([-2,16], [2*np.tan(self.psi), -16*np.tan(self.psi)], '--r')        
        line, = ax.plot([], [], 'o-', lw=2)
        time_text = ax.text(0.02, 0.95, '', transform=ax.transAxes)
        def animate(i):
            print('%1.2f/100' % (i*100/len(self.frames)))
            line.set_data(self.frames[i])
            time_text.set_text('time : %1.3f s' % (i*self.step) )
            return line, time_text
        def init():
            """initialize animation"""
            line.set_data([], [])
            time_text.set_text('')
            return line, time_text
        matplotlib.use("Agg")
        ## VIDEO CREATION
        ani = animation.FuncAnimation(fig,animate, frames = len(self.frames), blit = True, init_func=init)
        ani.save(self.name + '.mp4', fps=int(1/self.step), extra_args=['-vcodec', 'libx264'])
        matplotlib.use("Qt5Agg")
        return 1
        
simu = Simulation()
simu.launch(5, 0.001, np.array([[0],[-0.2]]),np.array([[0],[ 0]]), mode = 'euler', control = 'activ', controlMode = 'feedbackLinearisation')
simu.walker.phasePortrait.show() 
simu.walker.phasePortraitX.show() 
simu.walker.genCoordTimeEvolution.show()
simu.walker.speedGenCoordTimeEvolution.show()
simu.walker.command.show()
simu.walker.obs.show()
simu.walker.obsPsi.show()
simu.walker.energy.show()
simu.walker.poincare.show()
print(simu.walker.nbContacts)


def testAttractor() :
    q = np.linspace(-0.3,0.3, 100)
    qDot1 = 0
    qDot2 = 0
    a,b,c,d = -1,-1,-1,-1
    attraction = np.zeros((100,100))
    for q1 in q:
        a = (a +1)%100
        for q2 in q:
            b = (b +1)%100
            print(str(a + b*10**(-2)) + '/100')
            simu = Simulation()
            simu.launch(5, 0.001, np.array([[q1],[q2]]),np.array([[qDot1],[qDot2]]), mode = 'euler', control = 'activ')
            attraction[a,b] = simu.walker.nbContacts
    plt.figure()
    q1, q2 = np.meshgrid(q,q)
    plt.contourf(q1,q2,attraction, cmap = 'Reds')
    plt.colorbar()
    plt.xlabel('$q_1$ [rad]')
    plt.ylabel('$q_2$ [rad]')
    plt.show()
    return attraction








