#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 14:44:12 2019

@author: natienza
"""

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation

class Graph() :
    
    def __init__(self, name = '', xlabel = '', ylabel = '', plot1Label = 'Plot 1', plot2Label = 'Plot 2', plot3Label = 'Plot 3', *args, **kwargs):
        ## OPTIONS
        self.plot1Style = kwargs.get('plot1Style') if kwargs.get('plot1Style')!=None else '-'
        self.plot2Style = kwargs.get('plot2Style') if kwargs.get('plot2Style')!=None else '-'
        self.plot3Style = kwargs.get('plot3Style') if kwargs.get('plot3Style')!=None else '-'
        ## WHO AM I
        self.name = name
        ## METADATA
        self.xName = xlabel
        self.yName = ylabel
        self.plot1Name = plot1Label
        self.plot2Name = plot2Label
        self.plot3Name = plot3Label
        ## DATA
        self.x1 = []
        self.y1 = []
        self.x2 = []
        self.y2 = []
        self.x3 = []
        self.y3 = []
        

    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.name
    
    def show(self, save = 0):
        plt.figure()
        plt.plot(self.x1,self.y1,self.plot1Style, label = self.plot1Name)
        if self.x2 != [] :
            plt.plot(self.x2,self.y2,self.plot2Style, label = self.plot2Name)
            plt.legend()
        if self.x3!=[]:
            plt.plot(self.x3,self.y3,self.plot3Style, label = self.plot3Name)
            plt.legend()
        plt.title(self.name)
        plt.xlabel(self.xName)
        plt.ylabel(self.yName)
        plt.grid()
        plt.show()
        if save==1:
            plt.savefig(self.name + '.pdf')
        return 1


class Animation() :

    def __init__(self, psi = 0):
        ## WHO AM I
        self.name = '2D animation writer'
        ## ANIMATION DESCRIPTION
        self.psi = psi
        self.nbBody = 0
        self.frames = [[[0,0,0],[0,0,0]],[[0,0,0],[0,0,0]]]
        self.step = 0.01
        self.YcontactMap = []
        self.XcontactMap = []
    
    def setContactMap(self, contactX, contactY):
        self.XcontactMap = contactX
        self.YcontactMap = contactY
        
    def addFrame(self, frame):
        self.frames.append(frame)
    
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.name

    
    def create(self, name = ''):
        ## WHO AM I
        if name != '' :
            self.name = 'Video/' + name
        ## SETUP
        fig = plt.figure()
        ax = fig.add_subplot(111, aspect='equal', autoscale_on=False,
                             xlim=(-2, 16), ylim=(-6, 6))
        ax.grid()
        ax.plot(self.XcontactMap, self.YcontactMap, '--r')        
        line, = ax.plot([], [], 'o-', lw=2)
        time_text = ax.text(0.02, 0.95, '', transform=ax.transAxes)
        def animate(i):
            print('%1.2f/100' % (i*100/len(self.frames)))
            line.set_data(self.frames[i])
            time_text.set_text('time : %1.3f s' % (i*self.step) )
            return line, time_text
        def init():
            """initialize animation"""
            line.set_data([], [])
            time_text.set_text('')
            return line, time_text
        matplotlib.use("Agg")
        ## VIDEO CREATION
        ani = animation.FuncAnimation(fig,animate, frames = len(self.frames), blit = True, init_func=init)
        ani.save(self.name + '.mp4', fps=int(1/self.step), extra_args=['-vcodec', 'libx264'])
        matplotlib.use("Qt5Agg")
        return 1