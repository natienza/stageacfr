#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 14:51:53 2019

@author: natienza
"""

import numpy as np
import matplotlib.pyplot as plt

from simulation import Simulation
from agent import AgentSARSA, AgentSARSA_CV
from model import Walker

sarsa = AgentSARSA_CV()
simu = Simulation()
walker = Walker()

walker.controlMode = 'PD'

def discreteState(state):
    if state<=0:
        return 0
    if states[-1]<state:
        return states[-1]
    for i in range(len(states)-1):
        if states[i]<state and states[i+1] >state:
            if state-states[i]>states[i+1]-state:
                return states[i+1]
            return states[i]
        
states = np.arange(0,10,0.1)
for state in states:
    sarsa.stateActionValue[state]
for a in sarsa.actions:
    sarsa.stateActionValue[states[-1]][a] = 0
    
R = [0]
D = [0]
numEpisode = 200
reward = 0
for i in range(numEpisode):
    print(str((i+1)*100/numEpisode) + ' %')
    IC = [np.array([[0], [-0.2]]), np.array([[0],[0]])]
    state = discreteState(walker.l*np.sin(IC[0][0,0]))
    action1 = sarsa.chooseAction(state)
    isDone = False
    R += [0]
    D += [walker.xPoincareImpact]
    T = np.arange(0,5,0.01)
    walker.setIC(IC[0], IC[1])
    sarsa.reduceExploration()
    for t in T:
        walker.consigne = action1
        isDone = not(walker.simulateEuler(0.01, t, 'activ'))
        if isDone:
            sarsa.stateActionValue[state][action1] += -10
            break
        if walker.poincareImpact():
            reward = (walker.xPoincareImpact - walker.oldxPoincareImpact)**3 #+ walker.xPoincareImpact**4
            R[-1] += reward
            nextState = discreteState(walker.xHip)                    
            action2 = sarsa.chooseAction(nextState)
            sarsa.learn(state, action1, reward, nextState,action2)
            state = nextState
            action1 = action2

plt.figure()
plt.plot(range(len(R)), R)
plt.grid()
plt.xlabel('episode')
plt.ylabel('accumulative reward during episode')

plt.figure()
plt.grid()
plt.xlabel('episode')
plt.ylabel('walking distance during episode [m]')
plt.plot(range(len(D)), D)