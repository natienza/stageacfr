#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 14:42:29 2019

@author: natienza
"""

import numpy as np

from model import Walker
from agent import Agent


class Simulation() :

    def __init__(self):
        ## SIMULATION PARAMETERS
        self.time = []
        self.step = 0
        self.currentTime = 0
        ## MODEL OF ROBOT
        self.walker = Walker()
        ## CONTROL HIGH POLICY AGENT
        self.agent = Agent()
        ## WHO AM I
        self.name = 'Simulation of ' + self.walker.name

    
    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name
    
    def launch(self, duration = 30, step = 0.1, q0 = np.zeros((2,1)), qDot0 = np.zeros((2,1)), *args, **kwargs):
        mode = kwargs.get('mode')
        control = kwargs.get('control')
        controlMode = kwargs.get('controlMode') if kwargs.get('controlMode')!=None else 'PD'
        consigne = kwargs.get('action') if kwargs.get('action') != None else 0
        self.walker.anim.step = step
        self.step = step
        self.time = np.linspace(0,duration,int(duration/step)+1)
        self.walker.setIC(q0, qDot0)
        self.walker.controlMode = controlMode
        self.walker.consigne = consigne
        for t in self.time:
            if t == 0:
                pass
            if mode == 'euler':
                if not(self.walker.simulateEuler(step,t,control)):
                    return 0
                if self.walker.poincareImpact():
                    return (self.walker.xPoincareImpact - self.walker.oldxPoincareImpact) #reward
            elif mode == 'RK' :
                self.walker.simulateRK(step,t,control)
            elif mode == 'scipy':
                self.walker.simulateScipy(step,t,control)
            else :
                print('mode integration non connu')
                return 0
        return 0