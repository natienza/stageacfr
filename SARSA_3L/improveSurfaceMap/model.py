#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 14:43:15 2019

@author: natienza
"""

import numpy as np
import random as rd
from math import factorial
from numpy.linalg import inv
from numpy.linalg import pinv
from scipy.integrate import odeint

from plotting import Graph, Animation

class Walker() :
    
    def __init__(self, *args, **kwargs):
        ## WHO AM I
        self.name = '3L Compass gait biped walker'
        ## GEOMETRIC PARAMETERS
        self.a = 1.5
        self.b = 1.5
        self.l = self.a + self.b
        self.mH = 5.07#2
        self.m = 5#3/4
        ## GENERALIZED COORDINATES
        self.pEndEffector = [0,0]
        self.xHip = 0
        self.yHip = 0
        self.q = np.array([ [0],
                            [0]], dtype = np.float64)
        self.qDot = np.array([ [0],
                               [0]], dtype = np.float64)
        self.xq = np.array([0, 0, 0, 0], dtype = np.float64)
        ## STATE SPACE PARAMETERS
        self.x = np.array([ 0,0,0,0], dtype = np.float64)
        self.u = np.array([0],dtype = np.float64)
        self.a1 = 25
        self.a0 = 10
        self.y = 0
        self.v = 0
        ## CONTROL
        self.controlMode = kwargs.get('controlMode') if kwargs.get('controlMode')!=None else 'feedbackLinearisation'
        ## ENERGY
        self.k = 0
        self.V = 0
        ## DYNAMIC SINGLE SUPPORT MODEL
        self.M = np.array([ [self.mH*self.l**2+self.m*self.b**2+self.m*self.l**2, -self.m*self.l*self.a*np.cos(self.x[1])],
                            [-self.m*self.l*self.a*np.cos(self.x[1]), self.m*self.a**2]])
        
        self.C = np.array([ [0, self.m*self.l*self.a*self.qDot[1,0]*np.sin(self.x[1])],
                            [self.m*self.l*self.a*self.qDot[0,0]*np.sin(-self.x[1]), 0]])
    
        self.g = np.array([ [-(self.m*self.b + self.m*self.l + self.mH*self.l)*9.81*np.sin(self.x[0])],
                            [self.m*self.a*9.81*np.sin(self.q[1,0])]])
        ## IMPACT TRANSITION MATRIX
        self.psi = 0.1
        self.contact = 0 #contact flag
        self.oldq = np.array([ [0],
                            [0]], dtype = np.float64)
        self.eps = 0.001 #epsilon on the contact surface (disrete system)
        self.P = np.array([ [0, 1],
                            [1, 0]])
        self.Pq = np.linalg.solve(np.array([ [self.m*self.a**2 - self.m*self.a*self.l*np.cos(self.x[1]), self.m*self.l**2+self.mH*self.l**2+self.m*self.b**2 - self.m*self.a*self.l*np.cos(self.x[1])],
                                        [self.m*self.a**2, -self.m*self.a*self.l*np.cos(self.x[1])]], dtype = np.float64),np.array([ [(self.m*self.l*self.b + self.mH*self.l**2 + self.m*self.l*self.b)*np.cos(self.x[1]), -self.m*self.a*self.b],
                                                                                                                                        [-self.m*self.a*self.b, 0]], dtype = np.float64))
        ## VIRTUAL CONSTRAINTS
        self.lStep = 2
        self.tStep = 0.2
        self.consigne = 0.7254297928385293 #np.arcsin(self.lStep/(2*self.l))
        ## ATTRIBUTES REWARD
        self.xPoincareImpact = 0
        self.oldxPoincareImpact = 0
        ## DEBUGG ATTRIBUTES
        self.nbContacts = 0
        self.nbApprox = 0
        self.plotDiff = Graph()
        ## PLOT
        self.phasePortraitX = Graph('$\psi$ versus $q_1$', '$q_1$ [rad]', '$\psi$ [rad]')
        self.phasePortrait = Graph('Phase portrait','$q$ [rad]','$\dot{q}$ [rad/s]', '1', '2')
        self.genCoordTimeEvolution = Graph('State time evolution','t [s]','q [rad]' )
        self.speedGenCoordTimeEvolution = Graph('Generalized speed versus time', 't [s]', '$\dot{q}$ [rad/s]')
        self.command = Graph('Control input (hip torque versus time)', 't [s]', 'u [Nm]')
        self.obs = Graph('Observation output', 't [s]', 'y [rad]')
        self.obsPsi = Graph('$\psi$ versus time', 't [s]', '$\psi$ [rad]')
        self.commandeLin = Graph('linear command input', 't [s]', 'v [Nm]')
        self.energy = Graph('Energy versus time', 't [s]', 'Energy [J]', '$K$', '$V$', '$V-K$', plot3Style = '--')
        self.poincare = Graph('Poincare map', '$\dot{q_1}$ [rad]', '$\dot{q_2} [rad]$', plot1Style = '--o')
        ## ANIMATION
        self.reset = 0
        self.anim = Animation(self.psi)
        self.anim.nbBody = 2
        self.anim.setContactMap(np.linspace(0,17, 100), [self.surfaceMap4(t) for t in np.linspace(0,17,100)])
        
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.name

    def getStateVar(self):
        return self.x
    
    def getGenCoord(self):
        return self.q,self.qDot
    
    def f(self,y ,t):
        invM = self.inverse(self.M)
        dq = - np.dot(invM,self.C).dot(self.qDot) - invM.dot(self.g)
        return np.array([[self.qDot[0,0]],
                         [self.qDot[1,0]],
                         [dq[0,0]],
                         [dq[1,0]]])
    
    def setIC(self, q0, qDot0):
        self.q = q0
        self.oldq = q0
        self.qDot = qDot0
        self.xq[0] = q0[0,0]
        self.xq[1] = q0[1,0]
        self.xq[2] = qDot0[0,0]
        self.xq[3] = qDot0[1,0]
        self.x[0] = self.q[0,0]
        self.x[1] = self.q[1,0] - self.q[0,0]
        self.x[2] = self.qDot[0,0]
        self.x[3] = self.qDot[1,0] - self.qDot[0,0]
        self.updateDynamicParameters()
        self.reset = 1
        self.nbContacts = 0
        self.xPoincareImpact = 0
        self.oldxPoincareImpact = 0
        self.yHip = 0
        #print('IC set')
        return 1 
    
    def updateDynamicParameters(self, *args, **kwargs):
        if len(kwargs) != 0:
            X = kwargs.get('X')
            ''' X = np.array([q1,psi,q1Dot, psiDot])'''
            self.x = X
            self.xq = np.array([self.x[0], self.x[1]+self.x[0], self.x[2], self.x[2]+self.x[3]])
            self.q = np.array([[self.xq[0]],[self.xq[1]]])
            self.qDot = np.array([[self.xq[2]], [self.xq[3]]])
        ## DYNAMIC SINGLE SUPPORT MODEL
        self.M = np.array([ [self.mH*self.l**2+self.m*self.b**2+self.m*self.l**2, -self.m*self.l*self.a*np.cos(self.x[1])],
                            [-self.m*self.l*self.a*np.cos(self.x[1]), self.m*self.a**2]], dtype = np.float64)
        
        self.C = np.array([ [0, self.m*self.l*self.a*self.qDot[1,0]*np.sin(self.x[1])],
                            [self.m*self.l*self.a*self.qDot[0,0]*np.sin(-self.x[1]), 0]], dtype = np.float64)
    
        self.g = np.array([ [-(self.m*self.b + self.m*self.l + self.mH*self.l)*9.81*np.sin(self.x[0])],
                            [self.m*self.a*9.81*np.sin(self.q[1,0])]], dtype = np.float64)
        ## ENERGY
        self.k = 0.5 * self.m * ( self.b * self.x[2]**2 + (self.l*self.x[2]*np.sin(self.x[0]) + self.a*self.qDot[1,0]*np.sin(self.q[1,0]) )**2 + (self.l*self.x[2]*np.cos(self.x[0]) + self.a*self.qDot[1,0]*np.cos(self.q[1,0])) **2  ) + 0.5*self.mH*self.qDot[0,0]**2
        self.V =0.5 * self.m * (self.b + self.l) * np.cos(self.x[0]) + 0.5 * self.mH * (self.l*np.cos(self.x[0]) - self.a*np.cos(self.q[1,0]))
    
    def updateContactTransition(self):
        ## IMPACT TRANSITION MATRIX
        self.Pq = np.linalg.solve(np.array([ [self.m*self.a**2 - self.m*self.a*self.l*np.cos(self.x[1]), self.m*self.l**2+self.mH*self.l**2+self.m*self.b**2 - self.m*self.a*self.l*np.cos(self.x[1])],
                                        [self.m*self.a**2, -self.m*self.a*self.l*np.cos(self.x[1])]], dtype = np.float64).dot(self.P),np.array([ [(self.m*self.l*self.b + self.mH*self.l**2 + self.m*self.l*self.b)*np.cos(self.x[1]), -self.m*self.a*self.b],
                                                                                                                                        [-self.m*self.a*self.b, 0]], dtype = np.float64))

    def surfaceContact(self):
        if abs(self.pEndEffector[1] - self.surfaceMap4(self.pEndEffector[0])) <= 0.05:
            if abs(self.q[0,0] - self.q[1,0]) <= 0.2:
                return 0 # false contact : the two end points are colluding
            if  (-self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0])) < 0 :
                return 0  # false contact : we want to go forward
            if abs(-self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0])) < 0.2:
                return 0 # false contact
            ## ANIMATION UPDATE
            self.contact = 1
            ## CONTACT TRANSITION MODEL
            self.updateContactTransition()
            self.q = self.P.dot(self.q)
            self.qDot = self.Pq.dot(self.qDot)
            self.xq = np.array([self.q[0,0], self.q[1,0], self.qDot[0,0], self.qDot[1,0]])
            self.x = np.array([self.q[0,0],self.q[1,0]-self.q[0,0], self.qDot[0,0], self.qDot[1,0]-self.qDot[0,0]])
            self.nbContacts += 1
            return 1
        return 0
    
    def surfaceMap(self,x):
        ''' Surface de contact comprenant une rampe de pente 1 puis du plat'''
        if x<2:
            return 0
        elif x<3:
            return x-2
        else:
            return 1
        
    def surfaceMap2(self,x):
        '''Surface de contact comprenant une rampe de pente 1 puis un trou'''
        if x<2:
            return 0
        elif x<3:
            return x-2
        elif x<4:
            return 1
        elif x<5.5:
            return -2
        else:
            return 1
    
    def surfaceMap3(self, x):
        '''Surface de contact comprenant deux rampes de pente 1 --> pas adapte au compass gait biped robot'''
        if x<2:
            return 0
        elif x<3:
            return x-2
        elif x<5:
            return 1        
        elif x<6:
            return x-4
        else:
            return 2
        
    def surfaceMap4(self,x):
        '''Surface de contact plane comprenant 2 trous'''
        if x<3:
            return 0
        elif x<4:
            return -3
        elif x<5:
            return 0
        elif x<6:
            return -3
        else:
            return 0
        
        
    def poincareImpact(self):
        if (self.oldq[1,0] - self.oldq[0,0])*(self.q[1,0] - self.q[0,0])<0:
            self.oldxPoincareImpact = self.xPoincareImpact
            self.xPoincareImpact = self.xHip
            ## POINCARE MAP UPDATE
            self.poincare.x1.append(self.x[2])
            self.poincare.y1.append(self.qDot[1,0])
            return True
        return False
    
    def updatePlot(self,t):
        self.phasePortrait.x1.append(self.q[0,0])#%(2*np.pi))
        self.phasePortrait.y1.append(self.qDot[0,0])
        self.phasePortrait.x2.append(self.q[1,0])#%(2*np.pi))
        self.phasePortrait.y2.append(self.qDot[1,0])
        self.genCoordTimeEvolution.x1.append(t)
        self.genCoordTimeEvolution.y1.append(self.q[0,0])#%(2*np.pi))
        self.genCoordTimeEvolution.x2.append(t)
        self.genCoordTimeEvolution.y2.append(self.q[1,0])#%(2*np.pi))
        self.speedGenCoordTimeEvolution.x1.append(t)
        self.speedGenCoordTimeEvolution.y1.append(self.qDot[0,0])
        self.speedGenCoordTimeEvolution.x2.append(t)
        self.speedGenCoordTimeEvolution.y2.append(self.qDot[1,0])
        self.plotDiff.x1.append(t)
        self.plotDiff.y1.append((np.cos(self.oldq[0,0]) - np.cos(self.oldq[1,0]))*(np.cos(self.q[0,0]) - np.cos(self.q[1,0])))
        self.phasePortraitX.x1.append(self.x[0])
        self.phasePortraitX.y1.append(self.x[1])
        self.command.x1.append(t)
        self.command.y1.append(self.u[0])
        self.obs.x1.append(t)
        self.obs.y1.append(self.x[1] - self.psiD(self.x[0]))
        self.obsPsi.x1.append(t)
        self.obsPsi.y1.append(self.x[1])
        self.commandeLin.x1.append(t)
        self.commandeLin.y1.append(self.v)
        self.energy.x1.append(t)
        self.energy.y1.append(self.k)
        self.energy.x2.append(t)
        self.energy.y2.append(self.V)
        self.energy.x3.append(t)
        self.energy.y3.append(-self.k+self.V)
        ## UPDATE ANIMATION
        self.updateAnimation(t)
        return         
    
    def fsp2(self, X, t):
        ''' X = np.array([q1,psi,q1Dot, psiDot])'''
        self.updateDynamicParameters(X=X)
        dq = np.array([[X[2]],
                       [X[2]+X[3]]])
        invM = self.inverse(self.M)
        ddq = -invM.dot(self.C.dot(dq) + self.g)
        ddx = np.array([ddq[0,0], ddq[1,0]-ddq[0,0]])
        if self.control and (t/0.001)%5==0:
            if self.controlMode == 'feedbackLinearisation':
                self.v = -self.a0 * (X[3] - self.psiDDot(X[0])*X[2]) - self.a1*(X[1]-self.psiD(X[0]))
                self.u[0] = -ddx[1] + self.v + self.psiDDotDot(X[0])*X[2]**2 + self.psiDDot(X[0])*ddx[0]
            elif self.controlMode == 'PD':
                self.u[0] = -100 * (self.x[1] - self.consigne) - 10*self.x[3]
                if self.u[0]>100:
                    self.u[0] = 100
            ddx = np.array([ddx[0], ddx[1] + self.u[0]])
        return np.array([X[2], X[3], ddx[0], ddx[1]])
    
    def simulateEuler(self, step, t, mode='passiv'):
        for i in self.x:
            if i>10:
                return 0
        if self.yHip<0:
            return 0
        ## OPTIMIZE
        # implementation d'une surface de contact correcte sous la forme de f:x->y (e.g. y = rd.random()*0.2)
        if mode == 'passiv':
            self.control = False
        else :
            self.control = True
        ## SURFACE DE CONTACT
        self.surfaceContact()
        ## EULER INTEGRATION
        self.x =  (self.x + step*self.fsp2(self.x,t))
        self.xq = np.array([self.x[0], self.x[1]+self.x[0], self.x[2], self.x[2]+self.x[3]])
        ## DYNAMIC MODEL EVOLUTION
        self.oldq = self.q.copy()
        self.q = np.array([[self.xq[0]],[self.xq[1]]])
        self.qDot = np.array([[self.xq[2]], [self.xq[3]]])
        self.updateDynamicParameters()
        ## UPDATE PLOT
        self.updatePlot(t)
        return 1

    def simulateRK(self, step, t, mode='passiv'):
        if mode == 'passiv':
            self.control = False
        else :
            self.control = True
        ## SURFACE DE CONTACT
        self.surfaceContact()
        ## CONTROL
        self.controlFeedback()
        if np.linalg.det(self.M) == 0:
            print('ERROR MASS MATRIX INVERSION -- DEBUGG') #Should never happen according to spectral theorem
            invM = pinv(self.M)#SVD approx
            self.nbApprox += 1
        else:            
            invM = inv(self.M)    
        ## DYNAMIC MODEL
        self.oldq = self.q.copy()
        def f(X):
            dq = np.array([[X[2,0]],
                           [X[3,0]]], dtype = np.float64)
            dq += -invM.dot(self.C.dot(dq) + self.g)
            return np.array([[X[2,0]],
                             [X[3,0]],
                             [dq[0,0]],
                             [dq[1,0]]], dtype = np.float64)
        ## RUNGE-KUTTA INTEGRATION (order 4)
        Xq = np.array([[self.q[0,0]],
                       [self.q[1,0]],
                       [self.qDot[0,0]],
                       [self.qDot[1,0]]], dtype = np.float64)
        k1 = f(Xq)
        k2 = f(Xq+step*k1/2)
        k3 = f(Xq+step*k2/2)
        k4 = f(Xq + step*k3)
        Xq = Xq + step/6 * (k1 + 2*k2 + 2*k3 + k4)
        ## UPDATING
        self.q[0,0] = Xq[0,0]
        self.q[1,0] = Xq[1,0]
        self.qDot[0,0] = Xq[2,0]
        self.qDot[1,0] = Xq[3,0]
        self.x[0,0] = self.q[0,0] 
        self.x[1,0] = self.q[1,0] - self.q[0,0]
        self.x[2,0] = self.qDot[0,0]
        self.x[3,0] = self.qDot[1,0] - self.qDot[0,0] + b*self.u
        self.updateDynamicParameters()
        ## UPDATE PLOT
        self.updatePlot(t)
        return 1
    
    def fsp(self,X,t):
        ''' X = np.array([q1, q2, q1Dot, q2Dot])'''
        self.updateDynamicParameters(X = X)
        invM = self.inverse(self.M)
        dq = np.array([[X[2]],
                      [X[3]]])
        dq = -invM.dot(self.C.dot(dq) + self.g).copy()
        if self.control :
            self.v = -self.a0 * (X[3] - X[2] - self.psiDDot(X[0])*X[2]) - self.a1 * (X[1]-X[0] - self.psiD(X[0]))
            self.u = np.array([ -(dq[1,0] - dq[0,0]) + self.v + self.psiDDot(X[0])*dq[0,0] + self.psiDDotDot(X[0]) * X[2]**2])
            dq += np.array([[-self.u[0]], [self.u[0]]])
        return np.array([X[2], X[3], dq[0,0], dq[1,0]])
        
    
    def simulateScipy(self,step, t, mode = 'passiv'):
        if mode == 'passiv':
            self.control = False
        else :
            self.control = True
        ## CONTACT MAP
        self.surfaceContact()
        ## INTEGRATION
        integ = odeint(self.fsp2, self.x, np.array([t-step, t]))
        self.x = integ[-1,:].copy()
        ## UPDATING
        self.xq = np.array([self.x[0], self.x[1]+self.x[0], self.x[2], self.x[2]+self.x[3]])
        ## DYNAMIC MODEL EVOLUTION
        self.oldq = self.q.copy()
        self.q = np.array([[self.xq[0]],[self.xq[1]]])
        self.qDot = np.array([[self.xq[2]], [self.xq[3]]])
        self.updateDynamicParameters()
        ## UPDATE PLOT
        self.updatePlot(t)
        return 1
    
    
    def updateAnimation(self, t=0):
        lastRef = [self.anim.frames[-1][0][0],self.anim.frames[-1][1][0]]
        if self.reset:
            self.reset = 0
            lastRef = [0,0]
        if self.contact == 1:
            self.contact = 0
            lastRef[0] -= -self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0])  
            lastRef[1] -= self.l*(np.cos(self.q[0,0]) - np.cos(self.q[1,0]))
            #print(str(lastRef) + ' -> ' + str(t) )
        p1 = [-self.l*np.sin(self.q[0,0])+ lastRef[0], self.l*np.cos(self.q[0,0])+ lastRef[1]]
        p2 = [-self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0]) + lastRef[0], self.l*np.cos(self.q[0,0]) - self.l*np.cos(self.q[1,0]) + lastRef[1]]
        self.pEndEffector = p2
        self.xHip = p1[0]
        self.yHip = p1[1]
        self.anim.frames.append([[lastRef[0],p1[0],p2[0]],[lastRef[1],p1[1],p2[1]]])           
        return 1
    
    def psiD(self, q1) :
        psiLim = -np.arcsin(self.lStep/(2*self.l))
        s = (q1 +psiLim ) / (2 * psiLim)
        d = (2*psiLim)**2/(self.tStep*3)
        a = [2*psiLim,2*psiLim-d ,-2*psiLim +d,-2*psiLim]
        h=0
        for i in range (4):
           h += a[i] *  factorial(3)/(factorial(i)*factorial(3-i)) * s ** i * (1-s)**(3-i)
        return h   
        
    def psiDDot(self, q1) :
        ## derivate of psiD in function of q[0,0]
        psiLim = -np.arcsin(self.lStep/(2*self.l))
        s = (q1 +psiLim ) / (2 * psiLim)
        d = (2*psiLim)**2/(self.tStep*3)
        a = [2*psiLim,2*psiLim-d ,-2*psiLim +d,-2*psiLim]
        h=0
        for i in range (3):
           h += (a[i+1] - a[i]) *  factorial(3)/(factorial(i)*factorial(2-i)) * s ** i * (1-s)**(2-i)
        return h*(1/(2*psiLim))
    
    def psiDDotDot(self, q1):
        ## second derivate of psiD in function of q[0,0]
        psiLim = -np.arcsin(self.lStep/(2*self.l))
        s = (q1 +psiLim ) / (2 * psiLim)
        d = (2*psiLim)**2/(self.tStep*3)
        a = [2*psiLim,2*psiLim-d ,-2*psiLim +d,-2*psiLim]
        h=0
        for i in range (2):
           h += (a[i+2] -2*a[i+1] + a[i]) *  factorial(3)/(factorial(i)*factorial(1-i)) * s ** i * (1-s)**(1-i)
        return h*(1/(2*psiLim))**2
        
    @staticmethod
    def inverse(M):
        if np.linalg.det(M) == 0:
            print('ERROR MASS MATRIX INVERSION -- DEBUGG') #Should never happen according to spectral theorem
            invM = pinv(M) #SVD approx
        else:            
            invM = inv(M)   
        return invM   
    
walker = Walker()
walker.anim.create()