#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 14:48:42 2019

@author: natienza
"""

import numpy as np
import random as rd

from collections import defaultdict
from model import Walker

class Agent(Walker): 
    
    def __init__(self):
        super(Agent, self).__init__()
        ## WHO AM I 
        self.name = 'Agent'
        ## PARAMETERS
        self.step = 0.01
        self.actionValue = {}
        self.epsilon = 0.01
        self.gamma = 0.99
        self.nActions = 20
        ## INITIALIZE
        self.controlMode = 'PD'
        self.createSetAction()
        
    def __repr__(self):
        return self.name
    
    def __str__(self):
        return self.name
    
    def createSetAction(self):
        for i in range (self.nActions):
            self.actionValue[rd.random()] = 0
    
    @staticmethod
    def argMaxAction(dico):
        listQ = list(dico.values())
        maxQ = max(listQ)
        count = 0
        index = []
        for i in range(len(listQ)):
            q = listQ[i]
            if q == maxQ:
                count += 1
                index += [i]
        if count > 1:
            return list(dico.keys())[rd.sample(index,1)[0]]
        return list(dico.keys())[np.argmax(list(dico.values()))]
    
    def act(self, state, action):
        self.actionValue[action] = 0 if self.actionValue.get(action) == None else self.actionValue[action] #for debugg
        self.setIC(state[0], state[1])
        for t in np.arange(0,2,self.step):
            self.simulateEuler(self.step, t, 'activ')
            if self.poincareImpact():
                self.actionValue[action] += 1
                return 1
        self.actionValue[action] += -1
        return -1
    
    def choose(self, state):
        action = self.argMaxAction(self.actionValue)
        cpt = 0
        while self.act(state, action) < 0:
            action = self.argMaxAction(self.actionValue)
            cpt += 1
            if cpt >= 30:
                print('ERROR')
                break
        return action
    
class AgentSARSA():
    
    def __init__(self, *args, **kwargs):
        ## GET ATTRIBUTES
        self.name = kwargs.get('name') if kwargs.get('name') != None else 'SARSA agent'
        self.gamma = kwargs.get('gamma') if kwargs.get('gamma') != None else 0.99
        self.alpha = kwargs.get('alpha') if kwargs.get('alpha') != None else 0.5
        self.epsilon = kwargs.get('epsilon') if kwargs.get('epsilon') != None else 0.01
        self.step = kwargs.get('step') if kwargs.get('step') != None else 0.01
        self.numAction = kwargs.get('numAction') if kwargs.get('numAction') != None else 20
        ## INITIALIZE VALUE FUNCTION
        self.actions = np.linspace(0.1, 0.8, self.numAction)
        self.stateActionValue = defaultdict(lambda:{action : rd.random() for action in self.actions})
        ''' 
        stateActionValue = {state1:{action1:Q(state1,action1), action2:Q(state1,action2), ...}, state2:{...}}
            stateX -> float in [0,10] : position of the hip on the x-axis when impact om poicare map
            actionX -> float in [0.1, 0.8] : descritization of possible high level order
            Q(.,.) -> float : estimation of the value function for each action of each state
        '''
        
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.name
    
    @staticmethod
    def argMaxAction(dico):
        listQ = list(dico.values())
        maxQ = max(listQ)
        count = 0
        index = []
        for i in range(len(listQ)):
            q = listQ[i]
            if q == maxQ:
                count += 1
                index += [i]
        if count > 1:
            return list(dico.keys())[rd.sample(index,1)[0]]
        return list(dico.keys())[np.argmax(list(dico.values()))]
    
    
    def chooseAction(self, state):
        choice = None
        if self.epsilon == 0:
            choice = 0
        elif self.epsilon == 1:
            choice = 1
        else:
            r = rd.uniform(0,1)
            if r<self.epsilon:
                choice = 1
            else:
                choice = 0
            
        if choice == 1:
            return rd.choice(self.actions) #exploration
        else:
            return self.argMaxAction(self.stateActionValue[state]) #greedy exploitation    
    
    def learn(self, state1, action1, reward, state2, action2):
        ## SARSA UPDATE
        self.stateActionValue[state1][action1] += self.alpha * (reward + self.gamma * self.stateActionValue[state2][action2] - self.stateActionValue[state1][action1])
    