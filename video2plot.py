#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 14 10:04:43 2019

@author: natienza
"""

'''rouge = 1,0'''

import cv2
vidcap = cv2.VideoCapture('simulation3L/Video/PD_25Step.mp4')

frames = [] 

def getFrame(sec):
    vidcap.set(cv2.CAP_PROP_POS_MSEC,sec*1000)
    hasFrames,image = vidcap.read()
    if hasFrames:
        pass
        #cv2.imwrite("image"+str(count)+".jpg", image)     # save frame as JPG file
    return hasFrames, image

sec = 0
frameRate = 1.25 #//it will capture image in each xx second
count=1
success, img = getFrame(sec)
frames += [img]
while success:
    count = count + 1
    sec = sec + frameRate
    #sec = round(sec, 2)
    success, img = getFrame(sec)
    frames += [img]
    if sec >  262:
        pass#success = False


def merge(frames):
    res = frames[1]
    for i in range(2,len(frames)-1):
        res = cv2.bitwise_and(res, frames[i], frames[0])
    return res

cv2.imwrite('rapport/img/PD.jpg',merge(frames))
