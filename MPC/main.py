#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 14:51:53 2019

@author: natienza
"""

import numpy as np
import matplotlib.pyplot as plt

from simulation import Simulation
from agent import AgentSARSA, AgentSARSA_CV
from model import Walker
from predict import PredictionAgent

def discreteState(state):
    if state<=0:
        return 0
    if states[-1]<state:
        return states[-1]
    for i in range(len(states)-1):
        if states[i]<state and states[i+1] >state:
            if state-states[i]>states[i+1]-state:
                return states[i+1]
            return states[i]
        
states = np.arange(0,10,0.1)


simu = Simulation()
simu.launch(6, 0.001, np.array([[0],[0]]),np.array([[0],[0]]), mode = 'euler', control = 'activ')

#plt.figure()
#lastRef = [0.6000127712312482, 0.005119893813915644] 
#l = 3
#q = np.array([[-0.33983691], [0.33983691]])
#p1 = [-l*np.sin(q[0,0])+ lastRef[0], l*np.cos(q[0,0])+ lastRef[1]]
#p2 = [-l*np.sin(q[0,0]) + l*np.sin(q[1,0]) + lastRef[0], l*np.cos(q[0,0]) - l*np.cos(q[1,0]) + lastRef[1]]
#plt.plot([lastRef[0], p1[0], p2[0]], [lastRef[1], p1[1], p2[1]])
#
#lastRef = [1.766817797979996, 0.009001927466682935]
#l = 3
#q = np.array([[-0.43474467], [0.24610254]])
#p1 = [-l*np.sin(q[0,0])+ lastRef[0], l*np.cos(q[0,0])+ lastRef[1]]
#p2 = [-l*np.sin(q[0,0]) + l*np.sin(q[1,0]) + lastRef[0], l*np.cos(q[0,0]) - l*np.cos(q[1,0]) + lastRef[1]]
#plt.plot([lastRef[0], p1[0], p2[0]], [lastRef[1], p1[1], p2[1]])
#
#lastRef = [2.925445378135115, 0.008997013586940161]
#l = 3
#q = np.array([[-0.58445812], [0.11317348]])
#p1 = [-l*np.sin(q[0,0])+ lastRef[0], l*np.cos(q[0,0])+ lastRef[1]]
#p2 = [-l*np.sin(q[0,0]) + l*np.sin(q[1,0]) + lastRef[0], l*np.cos(q[0,0]) - l*np.cos(q[1,0]) + lastRef[1]]
#plt.plot([lastRef[0], p1[0], p2[0]], [lastRef[1], p1[1], p2[1]])
#
#
#plt.plot([i for i in range(6)],[simu.walker.surfaceMap2(i) for i in range(6)])
