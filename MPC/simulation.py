#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 14:42:29 2019

@author: natienza
"""

import numpy as np

from model import Walker
from agent import Agent
from predict import PredictionAgent

class Simulation() :

    def __init__(self):
        ## SIMULATION PARAMETERS
        self.time = []
        self.step = 0
        self.currentTime = 0
        ## MODEL OF ROBOT
        self.walker = Walker()
        ## PREDICTION AGENT
        self.agent = PredictionAgent(self.walker.surfaceMap2, lStep=2, l=3)
        ## WHO AM I
        self.name = 'Simulation of ' + self.walker.name

    
    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name
    
    def launch(self, duration = 30, step = 0.1, q0 = np.zeros((2,1)), qDot0 = np.zeros((2,1)), *args, **kwargs):
        mode = kwargs.get('mode')
        control = kwargs.get('control')
        self.walker.anim.step = step
        self.step = step
        self.time = np.linspace(0,duration,int(duration/step)+1)
        self.walker.setIC(q0, qDot0)
        oldNbContact = 0
        qf = np.array([[-0.5*self.walker.psiLim],[+0.5*self.walker.psiLim]])
        for t in self.time:
            if t == 0:
                pass
            if mode == 'euler':
                if not(self.walker.simulateEuler(step,t,control)):
                    return 0
                if self.walker.nbContacts > oldNbContact:
                    oldNbContact = self.walker.nbContacts
                    lastRef = np.array([[self.walker.oldpEndEffector[0]],
                                        [self.walker.oldpEndEffector[1]]])
                    qi = self.walker.q # we are after the contact transformation
                    qf = self.agent.predict(lastRef)
                    #print(q)
                    psif = qf[1,0]-qf[0,0]
                    psii = qi[1,0]-qi[0,0]
                    self.walker.q1p = qi[0,0]
                    self.walker.q1m = qf[0,0]
                    self.walker.param = [psii, psii - 18*psii/3, psif, psif]
            elif mode == 'RK' :
                self.walker.simulateRK(step,t,control)
            elif mode == 'scipy':
                self.walker.simulateScipy(step,t,control)
            else :
                print('mode integration non connu')
                return 0
        return