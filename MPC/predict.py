#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  6 18:00:25 2019

@author: natienza
"""

from plotting import Graph
from model import Walker

import numpy as np

class PredictionAgent:
    
    def __init__(self, surfaceMap=None, lStep = 1.5, l = 1.5, name = 'prediction agent', *args, **kwargs):
        ## WHO AM I
        self.name = kwargs.get('name') if kwargs.get('name')!=None else name
        ## MODEL
        self.l = kwargs.get('legLength') if kwargs.get('legLength')!=None else l
        self.lStep = kwargs.get('stepLength') if kwargs.get('stepLength')!=None else lStep
        self.contactMap = kwargs.get('contactMap') if surfaceMap == None else surfaceMap
        if self.contactMap == None:
            print('ERROR : contact map missing')
    
    def trial(self, x):
        return self.contactMap(x)
    
    def __repr__(self):
        return self.name
    
    def __str__(self):
        return self.name
    
    def computeNextRef(self, lastRef):
        return np.array([ [lastRef[0,0] +self.lStep], 
                          [self.contactMap(lastRef[0,0] + self.lStep)] ])
    
    @staticmethod
    def norme(u):
        ''' dim(u) = 2x1'''
        return  np.sqrt(u[0,0]**2 + u[1,0]**2)
    
    def predict(self, lastRef):
        endEffectorGoal = self.computeNextRef(lastRef)
        q = np.array([[-np.arcsin(self.lStep/(2*self.l))],
                      [np.arcsin(self.lStep/(2*self.l))]])
        endEffectorActual = np.array([[lastRef[0,0] - self.l*np.sin(q[0,0]) + self.l*np.sin(q[1,0])],
                                      [lastRef[1,0] + self.l*np.cos(q[0,0]) - self.l*np.cos(q[1,0])]])
        Q = 0.5 * self.norme(endEffectorActual-endEffectorGoal)**2
        dQ = np.array([[-self.l*np.cos(q[0,0]), -self.l*np.sin(q[0,0])],
                        [self.l*np.cos(q[1,0]), self.l*np.cos(q[1,0])] ]).dot(endEffectorActual-endEffectorGoal)
        alpha = 0.01
        while Q>0.0001:
            q -= alpha*dQ
            endEffectorActual = np.array([[lastRef[0,0] - self.l*np.sin(q[0,0]) + self.l*np.sin(q[1,0])],
                                      [lastRef[1,0] + self.l*np.cos(q[0,0]) - self.l*np.cos(q[1,0])]])
            Q = 0.5 * self.norme(endEffectorActual-endEffectorGoal)**2
            dQ = np.array([[-self.l*np.cos(q[0,0]), -self.l*np.sin(q[0,0])],
                        [self.l*np.cos(q[1,0]), self.l*np.cos(q[1,0])] ]).dot(endEffectorActual-endEffectorGoal)  

        return q
        
        