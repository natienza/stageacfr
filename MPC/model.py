#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 14:43:15 2019

@author: natienza
"""

import numpy as np
import random as rd
from math import factorial
from numpy.linalg import inv
from numpy.linalg import pinv
from scipy.integrate import odeint

from plotting import Graph, Animation

class Walker() :
    
    def __init__(self, *args, **kwargs):
        ## WHO AM I
        self.name = '3L Compass gait biped walker'
        ## GEOMETRIC PARAMETERS
        self.a = 1.5
        self.b = 1.5
        self.l = self.a + self.b
        self.mH = 5.07#2
        self.m = 5#3/4
        ## GENERALIZED COORDINATES
        self.q = np.array([ [0],
                            [0]], dtype = np.float64)
        self.qDot = np.array([ [0],
                               [0]], dtype = np.float64)
        self.xq = np.array([0, 0, 0, 0], dtype = np.float64)
        ## STATE SPACE PARAMETERS
        self.x = np.array([ 0,0,0,0], dtype = np.float64)
        self.u = np.array([0],dtype = np.float64)
        self.a1 = 40#25
        self.a0 = 65#10
        self.y = 0
        self.v = 0
        ## CONTROL
        self.controlMode = kwargs.get('controlMode') if kwargs.get('controlMode')!=None else 'feedbackLinearisation'
        ## ENERGY
        self.k = 0
        self.V = 0
        ## DYNAMIC SINGLE SUPPORT MODEL
        self.M = np.array([ [self.mH*self.l**2+self.m*self.b**2+self.m*self.l**2, -self.m*self.l*self.a*np.cos(self.x[1])],
                            [-self.m*self.l*self.a*np.cos(self.x[1]), self.m*self.a**2]])
        
        self.C = np.array([ [0, self.m*self.l*self.a*self.qDot[1,0]*np.sin(self.x[1])],
                            [self.m*self.l*self.a*self.qDot[0,0]*np.sin(-self.x[1]), 0]])
    
        self.g = np.array([ [-(self.m*self.b + self.m*self.l + self.mH*self.l)*9.81*np.sin(self.x[0])],
                            [self.m*self.a*9.81*np.sin(self.q[1,0])]])
        ## IMPACT TRANSITION MATRIX
        self.psi = 0
        self.contact = 0 #contact flag
        self.oldq = np.array([ [0],
                            [0]], dtype = np.float64)
        self.eps = 0.001 #epsilon on the contact surface (disrete system)
        self.P = np.array([ [0, 1],
                            [1, 0]])
        self.Pq = np.linalg.solve(np.array([ [self.m*self.a**2 - self.m*self.a*self.l*np.cos(self.x[1]), self.m*self.l**2+self.mH*self.l**2+self.m*self.b**2 - self.m*self.a*self.l*np.cos(self.x[1])],
                                        [self.m*self.a**2, -self.m*self.a*self.l*np.cos(self.x[1])]], dtype = np.float64),np.array([ [(self.m*self.l*self.b + self.mH*self.l**2 + self.m*self.l*self.b)*np.cos(self.x[1]), -self.m*self.a*self.b],
                                                                                                                                        [-self.m*self.a*self.b, 0]], dtype = np.float64))
        ## VIRTUAL CONSTRAINTS
        self.lStep = 2
        self.tStep = 0.2
        self.param =  [-2*np.arcsin(self.lStep/(2*self.l)), -2*np.arcsin(self.lStep/(2*self.l)) + 4/3*np.arcsin(self.lStep/(2*self.l)), 2*np.arcsin(self.lStep/(2*self.l)), 2*np.arcsin(self.lStep/(2*self.l))  ]
        self.psiLim = 2*np.arcsin(self.lStep/(2*self.l))
        self.q1p = 0.5*self.psiLim
        self.q1m = -0.5*self.psiLim
        ## LINEAR MODEL
        self.N = 5
        self.A = np.array([[1, 0.001],
                          [0, 1]])
        self.B = np.array([[0],
                           [0.001]])
        self.init_A_e()
        ## DEBUGG ATTRIBUTES
        self.nbContacts = 0
        self.nbApprox = 0
        self.plotDiff = Graph()
        self.flag = 0
        self.t = 0
        ## PLOT
        self.phasePortraitX = Graph('$\psi$ versus $q_1$', '$q_1$ [rad]', '$\psi$ [rad]')
        self.phasePortrait = Graph('Phase portrait','$q$ [rad]','$\dot{q}$ [rad/s]', '$q_1$ (stance leg)', '$q_2$ (swing leg)')
        self.genCoordTimeEvolution = Graph('State time evolution','t [s]','q [rad]', '$q_1$ (stance leg)', '$q_2$ (swing leg)' )
        self.speedGenCoordTimeEvolution = Graph('Generalized speed versus time', 't [s]', '$\dot{q}$ [rad/s]')
        self.command = Graph('Control input (hip torque versus time)', 't [s]', 'u [Nm]')
        self.inputEnergy = Graph('Input Energy', 't [s]', '$u^2$ [$Nm^2$]')
        self.obs = Graph('Observation output', 't [s]', 'y [rad]')
        self.obsPsi = Graph('$\psi$ versus time', 't [s]', '$\psi$ [rad]')
        self.commandeLin = Graph('linear command input', 't [s]', 'v [Nm]')
        self.energy = Graph('Energy versus time', 't [s]', 'Energy [J]', '$K$', '$V$', '$V+K$', plot3Style = '--')
        self.poincare = Graph('Poincare map', '$q_1$ [rad]', '$q_2 [rad]$', plot1Style = '--o')
        self.commandPredicted = Graph('Prediction','t [s]', 'u' )
        self.convergence = Graph('', 't [s]', '# iteration')
        self.hip = Graph('hip position', 'x [m]', 'y [m]')
        ## ANIMATION
        self.anim = Animation(self.psi)
        self.anim.nbBody = 2
        self.anim.setContactMap(np.linspace(0,17, 100), [self.surfaceMap2(t) for t in np.linspace(0,17,100)])
        
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.name

    def getStateVar(self):
        return self.x
    
    def getGenCoord(self):
        return self.q,self.qDot
    
    def f(self,y ,t):
        invM = self.inverse(self.M)
        dq = - np.dot(invM,self.C).dot(self.qDot) - invM.dot(self.g)
        return np.array([[self.qDot[0,0]],
                         [self.qDot[1,0]],
                         [dq[0,0]],
                         [dq[1,0]]])
    
    def setIC(self, q0, qDot0):
        self.q = q0
        self.oldq = q0
        self.qDot = qDot0
        self.xq[0] = q0[0,0]
        self.xq[1] = q0[1,0]
        self.xq[2] = qDot0[0,0]
        self.xq[3] = qDot0[1,0]
        self.x[0] = self.q[0,0]
        self.x[1] = self.q[1,0] - self.q[0,0]
        self.x[2] = self.qDot[0,0]
        self.x[3] = self.qDot[1,0] - self.qDot[0,0]
        self.updateDynamicParameters()
        self.oldpEndEffector = [0,0]
        self.pEndEffector = [-self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0]) , self.l*np.cos(self.q[0,0]) - self.l*np.cos(self.q[1,0])]
        #print('IC set')
        return 1 
    
    def updateDynamicParameters(self, *args, **kwargs):
        if len(kwargs) != 0:
            X = kwargs.get('X')
            ''' X = np.array([q1,psi,q1Dot, psiDot])'''
            self.x = X
            self.xq = np.array([self.x[0], self.x[1]+self.x[0], self.x[2], self.x[2]+self.x[3]])
            self.q = np.array([[self.xq[0]],[self.xq[1]]])
            self.qDot = np.array([[self.xq[2]], [self.xq[3]]])
        ## DYNAMIC SINGLE SUPPORT MODEL
        self.M = np.array([ [self.mH*self.l**2+self.m*self.b**2+self.m*self.l**2, -self.m*self.l*self.a*np.cos(self.x[1])],
                            [-self.m*self.l*self.a*np.cos(self.x[1]), self.m*self.a**2]], dtype = np.float64)
        
        self.C = np.array([ [0, self.m*self.l*self.a*self.qDot[1,0]*np.sin(self.x[1])],
                            [self.m*self.l*self.a*self.qDot[0,0]*np.sin(-self.x[1]), 0]], dtype = np.float64)
    
        self.g = np.array([ [-(self.m*self.b + self.m*self.l + self.mH*self.l)*9.81*np.sin(self.x[0])],
                            [self.m*self.a*9.81*np.sin(self.q[1,0])]], dtype = np.float64)
        ## ENERGY
        self.k = 0.5 * self.m * ( self.b * self.x[2]**2 + (self.l*self.x[2]*np.sin(self.x[0]) + self.a*self.qDot[1,0]*np.sin(self.q[1,0]) )**2 + (self.l*self.x[2]*np.cos(self.x[0]) + self.a*self.qDot[1,0]*np.cos(self.q[1,0])) **2  ) + 0.5*self.mH*self.qDot[0,0]**2
        self.V =0.5 * self.m * (self.b + self.l) * np.cos(self.x[0]) + 0.5 * self.mH * (self.l*np.cos(self.x[0]) - self.a*np.cos(self.q[1,0]))
    
    def updateContactTransition(self):
        ## IMPACT TRANSITION MATRIX
        self.Pq = np.linalg.solve(np.array([ [self.m*self.a**2 - self.m*self.a*self.l*np.cos(self.x[1]), self.m*self.l**2+self.mH*self.l**2+self.m*self.b**2 - self.m*self.a*self.l*np.cos(self.x[1])],
                                        [self.m*self.a**2, -self.m*self.a*self.l*np.cos(self.x[1])]], dtype = np.float64).dot(self.P),np.array([ [(self.m*self.l*self.b + self.mH*self.l**2 + self.m*self.l*self.b)*np.cos(self.x[1]), -self.m*self.a*self.b],
                                                                                                                                        [-self.m*self.a*self.b, 0]], dtype = np.float64))

#    def surfaceContact(self):
#        if (np.cos(self.oldq[0,0]+self.psi) - np.cos(self.oldq[1,0]+self.psi))*(np.cos(self.q[0,0]+self.psi) - np.cos(self.q[1,0]+self.psi)) <= 1e-5 and self.qDot[0,0]<=0:
#            if abs(self.q[0,0] - self.q[1,0]) <= 0.2:
#                return 0 # false contact : the two end points are colluding
#            if  (-self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0])) < 0 :
#                return 0  # false contact : we want to go forward
#            if abs(-self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0])) < 0.3:
#                return 0 # false contact
#            ## POINCARE MAP UPDATE
#            self.poincare.x1.append(self.x[0])
#            self.poincare.y1.append(self.x[2])
#            ## ANIMATION UPDATE
#            self.contact = 1
#            ## CONTACT TRANSITION MODEL
#            self.updateContactTransition()
#            self.q = self.P.dot(self.q)
#            self.qDot = self.Pq.dot(self.qDot)
#            self.xq = np.array([self.q[0,0], self.q[1,0], self.qDot[0,0], self.qDot[1,0]])
#            self.x = np.array([self.q[0,0],self.q[1,0]-self.q[0,0], self.qDot[0,0], self.qDot[1,0]-self.qDot[0,0]])
#            self.nbContacts += 1
#            return 1
#        return 0
    
    def poincareImpact(self):
        if (self.oldq[1,0] - self.oldq[0,0])*(self.q[1,0] - self.q[0,0])<0:
            return True
        return False    
    
    def surfaceContact(self):
        if abs(self.pEndEffector[1]-self.surfaceMap2(self.pEndEffector[0])) < 0.01:# and self.qDot[0,0]<=0:
            if abs(self.q[0,0] - self.q[1,0]) <= 0.2:
                return 0 # false contact : the two end points are colluding
            if  (-self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0])) < 0 :
                return 0  # false contact : we want to go forward
            if abs(-self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0])) < 0.2:
                pass#return 0 # false contact
            ## ANIMATION UPDATE
            self.contact = 1
            ## CONTACT TRANSITION MODEL
            self.updateContactTransition()
            self.q = self.P.dot(self.q)
            self.qDot = self.Pq.dot(self.qDot)
            self.xq = np.array([self.q[0,0], self.q[1,0], self.qDot[0,0], self.qDot[1,0]])
            self.x = np.array([self.q[0,0],self.q[1,0]-self.q[0,0], self.qDot[0,0], self.qDot[1,0]-self.qDot[0,0]])
            self.nbContacts += 1
            return 1
        return 0   
   
    def surfaceMap0(self,x):
        return 0
    
    def surfaceMap1(self,x):
        '''Surface de contact plane comprenant 2 trous'''
        if x<3:
            return 0
        elif x<4:
            return -3
        elif x<5:
            return 0
        elif x<6:
            return -3
        else:
            return 0
    
    def surfaceMap2(self,x):
        if x<3:
            return 0
        elif x<5:
            return -0.25*(x-3)
        elif x<9:
            return -0.5
        elif x<11:
            return 0.25*(x-11)
        else:
            return 0
    
    def updatePlot(self,t):
        self.phasePortrait.x1.append(self.q[0,0])#%(2*np.pi))
        self.phasePortrait.y1.append(self.qDot[0,0])
        self.phasePortrait.x2.append(self.q[1,0])#%(2*np.pi))
        self.phasePortrait.y2.append(self.qDot[1,0])
        self.genCoordTimeEvolution.x1.append(t)
        self.genCoordTimeEvolution.y1.append(self.q[0,0])#%(2*np.pi))
        self.genCoordTimeEvolution.x2.append(t)
        self.genCoordTimeEvolution.y2.append(self.q[1,0])#%(2*np.pi))
        self.speedGenCoordTimeEvolution.x1.append(t)
        self.speedGenCoordTimeEvolution.y1.append(self.qDot[0,0])
        self.speedGenCoordTimeEvolution.x2.append(t)
        self.speedGenCoordTimeEvolution.y2.append(self.qDot[1,0])
        self.plotDiff.x1.append(t)
        self.plotDiff.y1.append((np.cos(self.oldq[0,0]) - np.cos(self.oldq[1,0]))*(np.cos(self.q[0,0]) - np.cos(self.q[1,0])))
        self.phasePortraitX.x1.append(self.x[0])
        self.phasePortraitX.y1.append(self.x[1])
        self.command.x1.append(t)
        self.command.y1.append(self.u[0])
        self.inputEnergy.x1.append(t)
        self.inputEnergy.y1.append(self.u[0]**2)
        self.obs.x1.append(t)
        self.obs.y1.append(self.x[1] - self.psiD(self.x[0]))
        self.obsPsi.x1.append(t)
        self.obsPsi.y1.append(self.x[1])
        self.commandeLin.x1.append(t)
        self.commandeLin.y1.append(self.v)
        #self.commandeLin.x2.append(t)
        #self.commandeLin.y2.append(self.vvv)
        #self.commandeLin.x3.append(t)
        #self.commandeLin.y3.append(self.v)
        self.energy.x1.append(t)
        self.energy.y1.append(self.k)
        self.energy.x2.append(t)
        self.energy.y2.append(self.V)
        self.energy.x3.append(t)
        self.energy.y3.append(-self.k+self.V)
        #self.commandPredicted.x1.append(t)
        #self.commandPredicted.y1.append(self.vv)
        ## UPDATE ANIMATION
        self.updateAnimation(t)
        return         
    
    def fsp2(self, X, t):
        ''' X = np.array([q1,psi,q1Dot, psiDot])'''
        self.updateDynamicParameters(X=X)
        dq = np.array([[X[2]],
                       [X[2]+X[3]]])
        invM = self.inverse(self.M)
        ddq = -invM.dot(self.C.dot(dq) + self.g)
        ddx = np.array([ddq[0,0], ddq[1,0]-ddq[0,0]])
        #self.predictParam(X,ddx)
        if self.control and (t/0.001)%5==0:
            if self.controlMode == 'feedbackLinearisation':
                self.v = -self.a0 * (X[3] - self.psiDDot(X[0])*X[2]) - self.a1*(X[1]-self.psiD(X[0]))
                #self.vv = self.predict(X[1]-self.psiD(X[0]),X[3] - self.psiDDot(X[0])*X[2])
                #self.vvv = self.predict2(X[1]-self.psiD(X[0]),X[3])
                #self.uu = self.predictU3(X,ddx)
                self.u[0] = -ddx[1] + self.v + self.psiDDotDot(X[0])*X[2]**2 + self.psiDDot(X[0])*ddx[0]
                #self.vv = self.predict(X[1] - 0.6, X[3])
                #self.u[0] = -ddx[1] + self.vv
            elif self.controlMode == 'PD':
                self.u[0] = -50 * (self.x[1] - np.arcsin(self.lStep/(2*self.l))) - 10*self.x[3]
#            if self.u[0] > 100:
#                self.u[0] = 100
#            elif self.u[0]<-100:
#                self.u[0] = -100
            ddx = np.array([ddx[0], ddx[1] + self.u[0]])
        return np.array([X[2], X[3], ddx[0], ddx[1]])
    
    def simulateEuler(self, step, t, mode='passiv'):
        for i in self.x:
            if i>100:
                return 0
        if mode == 'passiv':
            self.control = False
        else :
            self.control = True
        ## SURFACE DE CONTACT
        self.surfaceContact()
        ## EULER INTEGRATION
        self.x =  (self.x + step*self.fsp2(self.x,t))
        self.xq = np.array([self.x[0], self.x[1]+self.x[0], self.x[2], self.x[2]+self.x[3]])
        ## DYNAMIC MODEL EVOLUTION
        self.oldq = self.q.copy()
        self.q = np.array([[self.xq[0]],[self.xq[1]]])
        self.qDot = np.array([[self.xq[2]], [self.xq[3]]])
        self.updateDynamicParameters()
        ## UPDATE PLOT
        self.updatePlot(t)
        return 1

    def simulateRK(self, step, t, mode='passiv'):
        if mode == 'passiv':
            self.control = False
        else :
            self.control = True
        ## SURFACE DE CONTACT
        self.surfaceContact()
        ## CONTROL
        self.controlFeedback()
        if np.linalg.det(self.M) == 0:
            print('ERROR MASS MATRIX INVERSION -- DEBUGG') #Should never happen according to spectral theorem
            invM = pinv(self.M)#SVD approx
            self.nbApprox += 1
        else:            
            invM = inv(self.M)    
        ## DYNAMIC MODEL
        self.oldq = self.q.copy()
        def f(X):
            dq = np.array([[X[2,0]],
                           [X[3,0]]], dtype = np.float64)
            dq += -invM.dot(self.C.dot(dq) + self.g)
            return np.array([[X[2,0]],
                             [X[3,0]],
                             [dq[0,0]],
                             [dq[1,0]]], dtype = np.float64)
        ## RUNGE-KUTTA INTEGRATION (order 4)
        Xq = np.array([[self.q[0,0]],
                       [self.q[1,0]],
                       [self.qDot[0,0]],
                       [self.qDot[1,0]]], dtype = np.float64)
        k1 = f(Xq)
        k2 = f(Xq+step*k1/2)
        k3 = f(Xq+step*k2/2)
        k4 = f(Xq + step*k3)
        Xq = Xq + step/6 * (k1 + 2*k2 + 2*k3 + k4)
        ## UPDATING
        self.q[0,0] = Xq[0,0]
        self.q[1,0] = Xq[1,0]
        self.qDot[0,0] = Xq[2,0]
        self.qDot[1,0] = Xq[3,0]
        self.x[0,0] = self.q[0,0] 
        self.x[1,0] = self.q[1,0] - self.q[0,0]
        self.x[2,0] = self.qDot[0,0]
        self.x[3,0] = self.qDot[1,0] - self.qDot[0,0] + b*self.u
        self.updateDynamicParameters()
        ## UPDATE PLOT
        self.updatePlot(t)
        return 1
    
    def fsp(self,X,t):
        ''' X = np.array([q1, q2, q1Dot, q2Dot])'''
        self.updateDynamicParameters(X = X)
        invM = self.inverse(self.M)
        dq = np.array([[X[2]],
                      [X[3]]])
        dq = -invM.dot(self.C.dot(dq) + self.g).copy()
        if self.control :
            self.v = -self.a0 * (X[3] - X[2] - self.psiDDot(X[0])*X[2]) - self.a1 * (X[1]-X[0] - self.psiD(X[0]))
            self.u = np.array([ -(dq[1,0] - dq[0,0]) + self.v + self.psiDDot(X[0])*dq[0,0] + self.psiDDotDot(X[0]) * X[2]**2])
            dq += np.array([[-self.u[0]], [self.u[0]]])
        return np.array([X[2], X[3], dq[0,0], dq[1,0]])
        
    
    def simulateScipy(self,step, t, mode = 'passiv'):
        if mode == 'passiv':
            self.control = False
        else :
            self.control = True
        ## CONTACT MAP
        self.surfaceContact()
        ## INTEGRATION
        integ = odeint(self.fsp2, self.x, np.array([t-step, t]))
        self.x = integ[-1,:].copy()
        ## UPDATING
        self.xq = np.array([self.x[0], self.x[1]+self.x[0], self.x[2], self.x[2]+self.x[3]])
        ## DYNAMIC MODEL EVOLUTION
        self.oldq = self.q.copy()
        self.q = np.array([[self.xq[0]],[self.xq[1]]])
        self.qDot = np.array([[self.xq[2]], [self.xq[3]]])
        self.updateDynamicParameters()
        ## UPDATE PLOT
        self.updatePlot(t)
        return 1
    
    
    def updateAnimation(self, t=0):
        lastRef = [self.anim.frames[-1][0][0],self.anim.frames[-1][1][0]]
        if self.contact == 1:
            self.contact = 0
            lastRef[0] -= -self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0])  
            lastRef[1] -= self.l*(np.cos(self.q[0,0]) - np.cos(self.q[1,0]))
            print(str(lastRef) + ' -> ' + str(t) )
        p1 = [-self.l*np.sin(self.q[0,0])+ lastRef[0], self.l*np.cos(self.q[0,0])+ lastRef[1]]
        p2 = [-self.l*np.sin(self.q[0,0]) + self.l*np.sin(self.q[1,0]) + lastRef[0], self.l*np.cos(self.q[0,0]) - self.l*np.cos(self.q[1,0]) + lastRef[1]]
        self.anim.frames.append([[lastRef[0],p1[0],p2[0]],[lastRef[1],p1[1],p2[1]]])           
        self.oldpEndEffector = self.pEndEffector
        self.pEndEffector = p2
        self.xHip = p1[0]
        self.yHip = p1[1]
        self.hip.x1.append(self.xHip)
        self.hip.y1.append(self.yHip)
        return 1
    
    def init_A_e(self):
        self.A_e = np.zeros((2*self.N,3*self.N))
        for i in range (0,2*self.N,2):
            self.A_e[i,i] = 1
            self.A_e[i+1, i+1] = 1
            if i>0:
                self.A_e[i,i-2] = -self.A[0,0]
                self.A_e[i,i-1] = -self.A[0,1]
                self.A_e[i+1,i-2] = -self.A[1,0]
                self.A_e[i+1,i-1] = -self.A[1,1]
            self.A_e[i+1][2*self.N+int(i/2)] = -self.B[1,0]
        self.H = np.zeros((3*self.N, 3*self.N))
        for i in range(3*self.N):
            if i>=2*self.N:
                 self.H[i,i] = 1
            if i%2 == 0:
                self.H[i,i] = self.a1
            else:
                self.H[i,i] = self.a0
    
    def predict(self,z1, z1Dot):
        b_e = np.zeros((2*self.N,2 ))
        b_e[0,1] = 0.001
        b_e[0,0] = 1
        b_e[1,1] = 1
        #b_e[0,0] = z1 + 0.001*z1Dot
        #b_e[1,0] = z1Dot
        inverse = inv(self.A_e.dot(inv(self.H).dot(self.A_e.transpose())))
        X = inv(self.H).dot(self.A_e.transpose().dot(inverse).dot(b_e)).dot(np.array([[z1],[z1Dot]]))
        #print( inv(self.H).dot(self.A_e.transpose().dot(inverse).dot(b_e))[self.N*2:-1, :])
        self.debugg = X
        #return -self.a1*z1 - self.a0*z1Dot
        return X[2*self.N,0]
    
    def predict2(self, z1, z1Dot):
        self.A1 = np.zeros((self.N*2, self.N*2))
        self.B1 = np.zeros((self.N*2,self.N))
        for i in range (0,2*self.N,2):
            self.A1[i,i] = 1
            self.A1[i+1, i+1] = 1
            if i>0:
                self.A1[i,i-2] = -self.A[0,0]
                self.A1[i,i-1] = -self.A[0,1]
                self.A1[i+1,i-2] = -self.A[1,0]
                self.A1[i+1,i-1] = -self.A[1,1]
            self.B1[i+1,int(i/2)] = -self.B[1,0]
        b_e = np.zeros((2*self.N, 1))
        b_e[0,0] = z1 + 0.001*z1Dot
        b_e[1,0] = z1Dot
        inverse = inv( (inv(self.A1).dot(self.B1)).transpose().dot(inv(self.A1).dot(self.B1)) )
        V = -inverse.dot((inv(self.A1).dot(self.B1)).transpose()).dot(inv(self.A1)).dot(b_e)
        return V[1,0]
    
    def predictU(self, X, ddX):
        ''' X = [q1, psi, q1Dot, psiDot]_k, ddX = [q1DotDot, psiDotDot]_k'''
        step = 0.001
        u = -ddX[1] - 1/step*( X[3] + self.psiDDot(X[0])*(X[2] + step*ddX[0]))
        return u
    
    def predictU2(self, X, ddX):
        ''' X = [q1, psi, q1Dot, psiDot]_k, ddX = [q1DotDot, psiDotDot]_k'''
        step = 0.001
        alpha = 10
        u = 0
        print('\n')
        while (X[3] + step*(ddX[1] + u) + self.psiDDot(X[0])*(X[2] + step*ddX[0]))**2 > 1:
            print((X[3] + step*(ddX[1] + u) + self.psiDDot(X[0])*(X[2] + step*ddX[0]))**2)
            d = 2*step*( X[3] + step*(ddX[1] + u) + self.psiDDot(X[0])*(X[2] + step*ddX[0]))
            u -= alpha*d
        return u
 
    def predictU3(self, X, ddX):
        ''' X = [q1, psi, q1Dot, psiDot]_k-1, ddX = [q1DotDot, psiDotDot]_k-1'''
        step = 0.001
        u = -1/step*( X[1] +2*step*X[3] + step**2*ddX[1] - 0.2)#self.psiD(X[0] + 2*step*X[2]) )
        return u  
    
    def predictParam(self, X, ddx):
        alpha = 0.0001
        self.param[1] = -2*np.arcsin(self.lStep/(2*self.l)) + 4/3*np.arcsin(self.lStep/(2*self.l))
        v = -self.a0 * (X[3] - self.psiDDot(X[0])*X[2]) - self.a1*(X[1]-self.psiD(X[0]))
        u = -ddx[1] + v + self.psiDDotDot(X[0])*X[2]**2 + self.psiDDot(X[0])*ddx[0]
        done = False
        cpt = 0
        while not(done):
            oldU = u
            du = u*(self.a0*self.d2(X[0])*X[2] + self.a1*self.d1(X[0]) + self.d1(X[0])*ddx[0] + self.d3(X[0])*X[2]**2 )
            oldParam = self.param[1]
            self. param[1] -= alpha*du
            if self.param[1] > self.param[0] - 2/3 *(-2*np.arcsin(self.lStep/(2*self.l))):
                self.param[1] = oldParam
                done = True
            v = -self.a0 * (X[3] - self.psiDDot(X[0])*X[2]) - self.a1*(X[1]-self.psiD(X[0]))    
            u = -ddx[1] + v + self.psiDDotDot(X[0])*X[2]**2 + self.psiDDot(X[0])*ddx[0]
            cpt += 1
            if abs(oldU**2 - u**2) < 3:
                done = True
        self.convergence.x1.append(self.t)
        self.convergence.y1.append(cpt)
        self.t += 0.001
    
    def predictParam2(self, X, ddx):
        alpha = 0.0001
        self.param[1] = -2*np.arcsin(self.lStep/(2*self.l)) + 4/3*np.arcsin(self.lStep/(2*self.l))
        v = -self.a0 * (X[3] - self.psiDDot(X[0])*X[2]) - self.a1*(X[1]-self.psiD(X[0]))
        u = -ddx[1] + v + self.psiDDotDot(X[0])*X[2]**2 + self.psiDDot(X[0])*ddx[0]
        du = u*(self.a0*self.d2(X[0])*X[2] + self.a1*self.d1(X[0]) + self.d1(X[0])*ddx[0] + self.d3(X[0])*X[2]**2 )
        oldDu = 0
        done = False
        while not(done):
            oldU = u
            oldParam = self.param[1]
            print(oldDu-du)
            self. param[1] -= 0.5*alpha*(oldDu-du)
            if self.param[1] > self.param[0] - 2/3 *(-2*np.arcsin(self.lStep/(2*self.l))):
                self.param[1] = oldParam
                done = True
            oldDu = du
            du = u*(self.a0*self.d2(X[0])*X[2] + self.a1*self.d1(X[0]) + self.d1(X[0])*ddx[0] + self.d3(X[0])*X[2]**2 )
            v = -self.a0 * (X[3] - self.psiDDot(X[0])*X[2]) - self.a1*(X[1]-self.psiD(X[0]))    
            u = -ddx[1] + v + self.psiDDotDot(X[0])*X[2]**2 + self.psiDDot(X[0])*ddx[0]
            if abs(oldU**2 - u**2) < 0.01:
                done = True
                
    
    def d1(self, q1):
        s = (q1 -self.q1p ) / (self.q1m-self.q1p)
        h = 0
        M = 3
        h = M*s*(1-s)**(M-1)
        return h
    
    def d2(self, q1):
        s = (q1 -self.q1p ) / (self.q1m-self.q1p)
        M = 3
        h = (1/(self.q1m-self.q1p))*(M*(1-s)**(M-1) - M*(M-1)*s*(1-s)**(M-2))
        return h
    
    def d3(self,q1):
        s = (q1 -self.q1p ) / (self.q1m-self.q1p)
        M = 3
        h = (1/(self.q1m-self.q1p))**2 * ( -2*M*(M-1)*(1-s)**(M-2) + M*s)
        return h
        
    def psiD(self, q1) :
        s = (q1 -self.q1p ) / (self.q1m-self.q1p)
        a = self.param
        h=0
        for i in range (4):
           h += a[i] *  factorial(3)/(factorial(i)*factorial(3-i)) * s ** i * (1-s)**(3-i)
        return h   
        
    def psiDDot(self, q1) :
        ## derivate of psiD in function of q[0,0]
        s = (q1 -self.q1p ) / (self.q1m-self.q1p)
        a = self.param
        h=0
        for i in range (3):
           h += (a[i+1] - a[i]) *  factorial(3)/(factorial(i)*factorial(2-i)) * s ** i * (1-s)**(2-i)
        return h*(1/(self.q1m-self.q1p))
    
    def psiDDotDot(self, q1):
        ## second derivate of psiD in function of q[0,0]
        s = (q1 -self.q1p ) / (self.q1m-self.q1p)
        a = self.param
        h=0
        for i in range (2):
           h += (a[i+2] -2*a[i+1] + a[i]) *  factorial(3)/(factorial(i)*factorial(1-i)) * s ** i * (1-s)**(1-i)
        return h*(1/(self.q1m-self.q1p))**2
        
    
    @staticmethod
    def inverse(M):
        if np.linalg.det(M) == 0:
            print('ERROR MASS MATRIX INVERSION -- DEBUGG') #Should never happen according to spectral theorem
            invM = pinv(M) #SVD approx
        else:            
            invM = inv(M)   
        return invM
